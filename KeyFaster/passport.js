var passport = require('passport');
var url = require('url');

function isLoggedIn(req, res, next){
    if (req.isAuthenticated()){
        req.cookies['user'] = req.user;
        req.session.return_to = "/dashboard";
        return next();
    } else {
        var userIDCookie = req.cookies['user'];
        //req.session.return_to = req.path;
        if (userIDCookie) {
            req.user = passport.deserializeUser(userIDCookie);
            console.log('userIDCoookie exists!! '+userIDCookie);
            req.session.return_to = "/dashboard";
            return next;
        }
        else {
            req.flash('message', 'Please sign-in to use that feature.');
            //req.session.return_to = req.path;
            req.session.return_to = req.path;//url.parse(req.url).pathname;
            console.log('--------------------------------------saving return path as: '+req.session.return_to);
            res.redirect('/');
        }
    }
};

module.exports = isLoggedIn;