var express = require('express');
var router = express.Router();
var passport = require('passport');
var isLoggedIn = require('../passport.js');

/* GET home page. */
router.get('/' || '/index', function(req, res, next) {
  res.render('pages/index', {user:req.user, message:req.flash('message')[0]});
});
//router.get('/dashboard/:sendingId', isLoggedIn, function(req,res, next){
//    res.render('pages/records', {user:req.user, message:req.flash('message')[0], loadEx:req.param('sendingId')});
//});
router.get('/dashboard', isLoggedIn, function(req,res, next){
    res.render('pages/records', {user:req.user, loadEx: 0,message:req.flash('message')[0]});
});
router.get('/settings', isLoggedIn, function(req, res, next){
    res.render('pages/settings', {user:req.user, message:req.flash('message')[0]});
});
router.get('/exercises', isLoggedIn, function(req, res, next){
    var tp = req.user.tpid;
    if (!tp)
        tp = 1;
    res.render('pages/exerciseBrowser', {tpid:tp, user:req.user, message:req.flash('message')[0]});
});
router.post('/setDefaultTP', isLoggedIn, function (req, res, next) {
    req.user.tpid = req.body.tpid;
    req.user.save();
    req.session.hasSetTP = true;
    res.end();
});
//router.get('/records', isLoggedIn, function(req, res, next){
//    //console.log('---------loading records: : '+JSON.stringify());
//    //getExerciseResultsForUser(req, res, req.cookies['user'].id, 1);
//    //console.log('results:::: ' + results);
//    res.render('pages/records', {user:req.user, exercises: req.user.exerciseData, message:req.flash('message')[0]});
//});

router.get('/oauth/facebook', passport.authenticate('facebook', {
    failureRedirect: '/',
    scope:['email']
}));

router.get('/oauth/facebook/callback', passport.authenticate('facebook', {
    failureRedirect: '/',
    failureFlash: 'Bad Twitter Login',
    successRedirect: '/return',
    scope:['email']
}));
router.get('/oauth/twitter', passport.authenticate('twitter', {
    failureRedirect: '/',
    failureFlash:'Bad Twitter Login'
}));

router.get('/oauth/twitter/callback', passport.authenticate('twitter', {
    failureRedirect: '/',
    failureFlash: 'Bad Twitter Login',
    successRedirect: '/return'
}));
router.get('/auth/google', passport.authenticate('google', {scope: ['openid email']}));
router.get('/auth/google/callback',
    passport.authenticate('google',
        {
            failureRedirect: '/',
            failureFlash: 'Failed Google Auth'
        }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/return');
    });
router.get('/auth/windowslive',
    passport.authenticate('windowslive', { scope: ['wl.signin', 'wl.basic', 'wl.emails'] }));

router.get('/auth/windowslive/callback',
    passport.authenticate('windowslive', { failureRedirect: '/' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/return');
    });
router.get('/return', isLoggedIn, function (req, res, next) {
    //console.log('returning to : ' + req.session.return_to);
    var returnTo = '/';
    if (req.session)
        if (req.session.hasSetTP) {
            if (req.user) {
                returnTo = req.session.return_to;
            }
        }
        else if (!req.session.hasSetTP)
        {
            returnTo = "/exercises"
        }
    res.redirect(returnTo || '/');//, {user:req.user, message:req.flash('message')[0]});
});
router.get('/getExData', isLoggedIn, function(req, res, next){
    res.json(req.user.exerciseData);
});
router.get('/exercises/one', isLoggedIn, function(req, res, next) {
    var tp = req.user.tpid;
    if (!tp)
        tp = 1;
        res.render('pages/exercises/exercise1', {tpid:tp, user:req.user, message:req.flash('message')[0]});
});

router.post('/exercises/submit', isLoggedIn, function (req, res, next) {
    var jsob = req.body.exData;
    jsob = JSON.parse(jsob);
    global.saveExerciseData(req, res, req.cookies['user'].id, {
        exercise: (+jsob.exercise+1).toString(),
        completionTime: jsob.completionTime,
        rate: jsob.rate,
        missedKeys: jsob.missedKeys
        //score: jsob.score || 0
    },jsob.missedKeys, next);
});

router.get('/exercises/two', isLoggedIn, function(req, res, next) {
    var tp = req.user.tpid;
    if (!tp)
        tp = 1;
    res.render('pages/exercises/exercise2', {tpid:tp,user:req.user, message:req.flash('message')[0]});
});
//router.post('/exercises/two', isLoggedIn, function (req, res, next) {
//
//    global.saveExerciseData(req, res, req.cookies['user'].id, {
//        exercise: req.body.exercise,
//        completionTime: req.body.completionTime,
//        rate: req.body.rate
//    },req.body.missedKeys, next);
//});
//
router.get('/exercises/three', isLoggedIn, function(req, res, next) {
    var tp = req.user.tpid;
    if (!tp)
        tp = 1;
    res.render('pages/exercises/exercise3', {tpid:tp,user:req.user, message:req.flash('message')[0]});
});
//router.post('/exercises/three', isLoggedIn, function (req, res, next) {
//    global.saveExerciseData(req, res, req.cookies['user'].id, {
//        exercise: req.body.exercise,
//        completionTime: req.body.completionTime,
//        rate: req.body.rate
//    },req.body.missedKeys, next);
//});
//
router.get('/exercises/four', isLoggedIn, function(req, res, next) {
    var tp = req.user.tpid;
    if (!tp)
        tp = 1;
    res.render('pages/exercises/exercise4', {tpid:tp,user:req.user, message:req.flash('message')[0]});
});
//router.post('/exercises/four', isLoggedIn, function (req, res, next) {
//    global.saveExerciseData(req, res, req.cookies['user'].id, {
//        exercise: req.body.exercise,
//        completionTime: req.body.completionTime,
//        rate: req.body.rate
//    },req.body.missedKeys, next);
//});


    /* Handle Logout */
    router.get('/signout', function(req, res) {
        req.logout();
        res.clearCookie('userId');
        res.redirect('/#!index');
    });

    /* Handle Login POST */
    router.post('/login', passport.authenticate('login', {
        successRedirect: '/return',
        failureRedirect: '/',
        failureFlash : 'Invalid Username or Password'
    }));

    /* GET Registration Page */
    router.get('/signup', function(req, res){
        res.render('pages/register',{message:req.flash('message')[0]});
    });

    /* Handle Registration POST */
    router.post('/signup', passport.authenticate('signup', {
        successRedirect: '/return',
        failureRedirect: '/signup',
        failureFlash : 'User already exists!'
    }));


module.exports = router;
