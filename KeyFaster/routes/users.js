var express = require('express');
var router = express.Router();
var passport = require('passport');
var isLoggedIn = require('../passport.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.post('/updateEmail', isLoggedIn, passport.authenticate('login', {
    successRedirect: '/dashboard',
    failureRedirect: '/',
    failureFlash : 'Invalid Username or Password'
}));
module.exports = router;
