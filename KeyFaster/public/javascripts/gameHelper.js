/**
 * Created by Daniel on 5/12/2015.
 */
var images, loader, canvas, timerDisplay, stage;
var running = false, gameTimer = 0, frameCount = 0, pressedKeys = [], whichercise = "1", lastKeyPressed = "N/A", canter = 1;
var fox = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];
var text7 = ['w','1','s','a','3','e','1','f','r','a','d','s','r','w','4','a','2','s','w','f','a','r','d','r','d','w','s'];
var text8 = ['q','r','w','e','t','q','e','d','f','r','4','e','3','w','1','q'];
var text9 = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];
var text10 = ['t','h', 'e','r', 'o','n', 'a','n', 'r','e', 'h','e', 'i','n', 'e','d', 'n','d', 'h','a', 'a','t', 'e','n', 'e','s', 'o','f', 'o','r', 'n','t', 'e','a', 't','i', 't','o', 'i','t', 's','t', 'i','o', 'l','e', 'i','s', 'o','u', 'a','r', 'a','s', 'd','e', 'r','t', 'v','e'];
var text11 = ['t','h','e', 'a','n','d', 't','h','a', 'e','n','t', 'i','o','n', 't','i','o', 'f','o','r', 'n','d','e', 'h','a','s', 'n','c','e', 'e','d','t', 't','i','s', 'o','f','t', 's','t','h', 'm','e','n'];
var text12 = ['t','h', 'e','r', 'o','n', 'a','n', 'r','e', 'h','e' ];
var updateEvent = new CustomEvent(
    "updategame",
    {
        detail: {
            message: "updating"
        },
        bubbles: true,
        cancelable: true
    }
);
function setSpeed(id)
{
    id = id.value;
    baseVelocity = id.valueOf();
}
function setText(id) {
    //if (!running)
    //{
        id = id.value;
        if (id === "1") {
            fox = text9;
            canter = 2;
        }
        else if (id === "2"){
            fox = text7;
            canter = 3;
        }
        else if (id === "3"){
            fox = text8;
            canter = 2;
        }
        else if (id === "4"){
            fox = text10;
            canter = 2;
        }
        else if (id === "5"){
            fox = text11;
            canter = 3;
        }
        else if (id === "6"){
            fox = text12;
            canter = 2;
        }

        whichercise = id;


}
function hideSelect(isRunning)
{
    //set select box to disabled
    document.getElementById('trainerSelector').disabled = isRunning;
    var sl = document.getElementById('speedSelector');
    if (sl)
     sl.disabled = isRunning;
}
function getChar(event) {

    if (event.which == null) {
        return String.fromCharCode(event.keyCode); // IE
    } else if (event.which != 0 && event.charCode != 0) {
        //console.log(String.fromCharCode(event.which));
        return String.fromCharCode(event.which);  // the rest
    } else {
        return null; // special key
    }
    event.preventDefault();
    return event;
    //preventDefault(event);
}
function handleInput(event)
{
    var keypressed = getChar(event);
    if (keypressed === " ")
        keypressed = "space";

    //pressedKeys.push(keypressed);
    pressedKeys[0] = keypressed;
    lastKeyPressed = keypressed;
    return keypressed;
}

function resetGameTimer()
{
    frameCount = 0;
}
function runGameTimer() {
    if (running || tumbling) {
        frameCount += 1;
        if (frameCount % (30 / 10) === 0) {
            gameTimer = frameCount / (30);
            timerDisplay.text = "Time Elapsed: " + gameTimer +"";
        }
       if (typeof letterContainer != 'undefined')
           letterContainer.dispatchEvent(updateEvent);
    }
}
function tweenComplete(tween)
{
    //tween.destroy();
    console.log("tween complete");
}
function tweenObj(myObject, desX, desY) {
    myTween = createjs.Tween.get(myObject,{loop:false, override:true})
        .wait(0)
        .to({x:desX, y:desY, rotation:0}, 500)
        .call(tweenComplete);

}
function tweenLetter(myObject, desX, desY, velocity)
{
    var letterTween = createjs.Tween.get(myObject, {loop:false})
        .wait(0)
        .to({x:desX, y:desY, rotation:0}, (+velocity*230))
        .call(tweenComplete);
}
function tweenFade(myObject, ttl)
{
    var fadeTween = createjs.Tween.get(myObject,{loop:false, override:true})
        .wait(+ttl/2)
        .to({alpha:0},(+ttl/2))
        .call(tweenComplete);
}
//function tweenFlip(myObject, speed, backUp)
//{
//    var xArc = 180;
//    var yArc = 180;
//
//    var tFlip = TweenLite.to(myObject, 1, {scaleX:0.01, z:-300, oncomplete: flipComplete});
//        //var tReturn = TweenLite.to(myObject, 1, {scaleX:1, z:0})
//
//    function flipComplete(event)
//    {
//        var tmax = TweenLite.to(myObject, 1, {delay:1,scaleX:1, z:0})
//    }
//    //var tmax = TweenMax.to(myObject, 1, {x: 500, y:500, transformPerspective:200, rotateX:360});
//
//    console.log('flipping after clicking '+ myObject.x + ', '+myObject.y);
//}
function updateGame(event)
{

    if (running)
        runGameTimer();
    if (stage)
        stage.update();
}
function setupCanvas(bgColor)
{
    canvas = document.getElementById('canvas');
    canvas.width = 800;
    canvas.height = 600;
    stage = new createjs.Stage(canvas);
    var background = new createjs.Shape();
    background.graphics.beginFill(bgColor).drawRect(0, 0, canvas.width, canvas.height);
    background.x = 0;
    background.y = 0;
    stage.addChild(background);
    letterContainer = new createjs.Container();
    //stage.addChild()

}
function getSelectionText()
{
    var el = document.getElementById('trainerSelector');
    if (el)
    {
        return el[el.selectedIndex].innerHTML;
    }
}
function checkCollision(obj1, obj2) {
    if (obj1.hitTest(obj2.x, obj2.y))
        return true;
}
function preventDefault(e)
{
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = e;
}
(function (window) {
    function Letter() {

        this.initialize();
    }

    //var tickCounter = 0;
    //Inheritance from Container
    Letter.prototype = new createjs.Container();
    Letter.prototype.Container_initialize = Letter.prototype.initialize;
    Letter.prototype.Container_tick = Letter.prototype._tick;

    Letter.prototype.initialize = function () {
        //call to initialize() method from parent class
        this.Container_initialize();

        //console.log("Letter initialized");
    };
    Letter.prototype._tick = function () {
        //call to _tick method from parent class
        this.Container_tick();

        //console.log("Letter Ticked");
    };

    Letter.prototype.onKeypress = function (event) {
        //handleInput(event);

        preventDefault(event);
    };
    Letter.prototype.update = function () {
        //winning some points

    };

    window.Letter = Letter;
}(window));

function post(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function init(files)
{
    return function () {


        loader = new createjs.LoadQueue(true);
        createjs.Sound.alternateExtensions = ["wav"];
        loader.installPlugin(createjs.Sound);
        if (files && files.constructor === Array && files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                loader.loadFile({id:files[i].split('/')[2], src:files[i]});
            }
        }
        images = images || {};
        //var manifest = {
        //src: '../images/cardback.png',
        //id: 'cardback'
        //}

        //loader.on('fileload', handleFileLoad, this);
        //loader.loadFile(manifest);
        loader.on('complete', main, this);
        loader.load();

        createjs.Ticker.on("tick", updateGame);
        createjs.Ticker.setFPS(30);
    }
}

