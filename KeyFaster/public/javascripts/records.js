
var exData = 'hotpotato';
var myGraph, myTGraph, expos;
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
function mymain()
{
    //onResize();
    //Chart.defaults.global.responsive = true;
//            var keyboard = new createjs.Bitmap("./keyboard.sv")
//    jqAjax(document).ready(function(){
//
//    });



    function getColor()
    {
        return (25+(Math.random()*205));
    }
    function removeOldMetrics()
    {
        document.getElementById('metricsContainer').innerHTML = '';
    }
    function setGraph(targetExercise)
    {
        removeOldMetrics();
        addKeyboardToPage(targetExercise);
        addMetricsListener(+targetExercise.exercise);
        var tlabels = [];
        var tdatasets = [], cdatasets = [];
        var avgR = [];
        var avgC = [];
        var bestR = [];
        var bestC = [];
        if (myGraph)
            myGraph.destroy();
        if (myTGraph)
            myTGraph.destroy();
        var rategraph = document.getElementById('rategraph').getContext('2d');
        var timegraph = document.getElementById('timegraph').getContext('2d');

        for (var i = 0; i < targetExercise.history.length; i++)
        {
            var index = (targetExercise.history.length - i);
            //in here create a line for each stat to track (avg, best, avg, best)
            avgR.push(targetExercise.history[i].avgRate);
            avgC.push(targetExercise.history[i].avgCompletionTime);
            bestR.push(targetExercise.history[i].bestRate);
            bestC.push(targetExercise.history[i].bestTime);
            console.log('xCompleted:: '+targetExercise.xCompleted);
            index = ''+(+targetExercise.xCompleted-(+index)+1)+'';
            //index = (+targetExercise.xCompleted + +index)+"";
            if ((!index.endsWith(0)) && index != 11 && index.endsWith(1))
            {
                tlabels.push(index+"st");
            }
            else if (index != 12 && index.endsWith(2))
            {
                tlabels.push(index+"nd");
            }
            else if (index != 13 && index.endsWith(3))
            {
                tlabels.push(index+"rd");
            }
            else
            {
                tlabels.push(index+"th");
            }
        }
        //var ddata =
        //{
        //    fillColor : "rgba("+getColor()+","+getColor()+","+getColor()+",0)",
        //    strokeColor : "rgba("+getColor()+","+getColor()+","+getColor()+",1)",
        //    pointColor : "#fff",
        //    pointStrokeColor : "#000",
        //    data : [12,60,5,23,99]
        //};
        tdatasets = [
            {
                fillColor : "#EB7F00",
                opacity: 0.4,
                strokeColor : "rgba(0,0,0,0.6)",
                pointColor : "#f1a54c",
                pointStrokeColor : "#000",
                label: "Best Rate",
                data : bestR
            },
            {
                fillColor : "#7f00eb",
                opacity: 0.4,
                strokeColor : "rgba(0,0,0,0.6)",
                pointColor : "#a54cf1",
                pointStrokeColor : "#000",
                label : "Average Rate",
                data : avgR
            }];
        cdatasets= [
            {
                label:"Average Time",
                fillColor : "#7f00eb",
                opacity: 0.4,
                strokeColor : "rgba(0,0,0,0.6)",
                pointColor : "#a54cf1",
                pointStrokeColor : "#000",
                data : avgC
            },
            {
                label: "Best Time",
                fillColor : "#EB7F00",
                opacity: 0.4,
                strokeColor : "rgba(0,0,0,0.6)",
                pointColor : "#f1a54c",
                pointStrokeColor : "#000",
                data : bestC
            }
        ];
        if (!targetExercise.history[0])
        {

            rategraph.fillText("No Data Available", rategraph.canvas.width/3, rategraph.canvas.height/2);
            rategraph.font = "48px Arial";
            timegraph.fillText("No Data Available", timegraph.canvas.width/3, timegraph.canvas.height/2);
            timegraph.font = "48px Arial";

        }
        else {
            var chartData = {
                labels: tlabels,//["1st Attempt", "5th Attempt", "10th Attempt", "15th Attempt"],
                datasets: tdatasets
            };
            var chart2 =
            {
                labels: tlabels,
                datasets: cdatasets
            };
            if(myGraph)
                myGraph.destroy();
            myGraph = new Chart(rategraph).Line(chartData, {
                multiTooltipTemplate: "<%= datasetLabel %>: <%= value %> WPM",
                percentageInnerCutout : 70,
                //responsive:true,
                //maintainAspectRatio: false,
                legendTemplate : '<ul>'
                +'<% for (var i=0; i<datasets.length; i++) { %>'
                +'<li>'
                +'<span style=\"background-color:<%=datasets[i].lineColor%>\"></span>'
                +'<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                +'</li>'
                +'<% } %>'
                +'</ul>'
            });
            if(myTGraph)
                myTGraph.destroy();
            //var scaleVal = (+targetExercise.history[targetExercise.history.length-1].bestTime / 2) || 1;
            //var scaleStartVal = (+targetExercise.history[targetExercise.history.length-1].bestTime - 3) || 1;
            //var scaleStepCount = (+targetExercise.history[0] - +targetExercise.history[targetExercise.history.length-1].bestTime);
            //if (scaleStepCount < 1)
            //    scaleStepCount = 1;

            myTGraph = new Chart(timegraph).Line(chart2, {
                //scaleOverride: true,
                //scaleSteps: scaleStepCount,
                //scaleStepsWidth: scaleVal,
                //scaleStartValue:scaleStartVal,
                //responsive: true,
                //scaleSteps: (+targetExercise.history[0].bestTime + 5),
                //scaleStepWidth: 1,
                //scaleStartValue: scaleVal,
                percentageInnerCutout : 70,
                multiTooltipTemplate: "<%= datasetLabel %>: <%= value %> seconds",
                legendTemplate : '<'+'ul style="margin:0 auto;">'
                +'<% for (var i=0; i<datasets.length; i++) { %>'
                +'<'+'li>'
                +'<'+'span style=\"background-color:<%=datasets[i].lineColor%>\"></span>'
                +'<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                +'<'+'/li>'
                +'<% } %>'
                +'<'+'/ul>'
            });
        }
        console.log(targetExercise);
    }
    function setKeyboardPrompt(label, i)
    {
        var prelabel = '';
        var postlabel = '';
        console.log('Label: '+label);
        switch (label) {
            case 1: postlabel = 'Full Alphabet';break;
            case 2: postlabel = 'WASD Training';break;
            case 3: postlabel = 'MOBA Training';break;
            case 4: postlabel = 'Muscle Mastery 1';break;
            case 5: postlabel = 'Muscle Mastery 2';break;
            case 6: postlabel = 'Capstone';break;
            default: postlabel = label;
        }
        switch (i) {
            case 1: prelabel = 'SplatterKeys - '; break;
            case 2: prelabel = 'Point and Pop - '; break;
            case 3: prelabel = 'Typing for Apples - '; break;
            case 4: prelabel = 'Keyboard Hero - '; break;
            default: break;
        }
        prelabel += postlabel;
        console.log('final label: '+prelabel);
        document.getElementById('keyboardPrompt').innerHTML = prelabel;
    }
    function poop()
    {
        for (var i = 1; i < 5; i++)
        {
            var foundOne = false;
            for (var j = 1; j < 7; j++)
            {
                var el = document.getElementById('tp'+i+''+j);
                //var targetExercise = el.getAttribute('ex');
                if (!el)
                    break;
                var mexData = exData[el.getAttribute('ex')-1];

                if (!mexData || mexData.xCompleted < 1)
                {
                    el.style.display = 'none';
                }
                else if (mexData.xCompleted >= 1) {
                    el.style.display = 'block';
                    foundOne = true;

                    (function (el, i, j) {
                        document.addEventListener('click', function (event) {
                            if (event.target === el) {

                                setGraph(exData[el.getAttribute('ex') - 1]);
                                //set the keyboardPrompt text to current exercise/tp combo
                                setKeyboardPrompt(j,i);

                            }
                        });
                    })(el, i, j);
                }
            }
            if (!foundOne) {
                var mel = document.createElement('div');
                mel.id = 'emptyTps';
                mel.className = 'tps';
                mel.addEventListener('click', goToExercise(i));
                mel.innerHTML = "No Data Available Yet";
                mel.style.display = 'block';
                mel.style.zIndex = '999';
                el.parentNode.appendChild(mel);

                console.log('appending mel');
            }
            // else  {
            //    var nday = document.getElementById('emptyTps');
            //    if (nday)
            //        nday.parentNode.removeChild(nday);
            //}
        }
    }
    function goToExercise(ex)
    {

        return function(){
            switch (ex){
                case 1: {
                    window.location = "/exercises/one";
                    break;
                }
                case 2: {
                    window.location = "/exercises/two";
                    break;
                }
                case 3: {
                    window.location = "/exercises/three";
                    break;
                }
                case 4: {
                    window.location = "/exercises/four";
                    break;
                }
                case 5: {
                    window.location = "/exercises/five";
                    break;
                }
                default : {
                    break;
                }
            }

        };
    }
    //addKeyboardToPage();
    function addKeyboardToPage(exercise) {
        var index = exercise.exercise;
        expos = +index - 1;
        console.log('expos: ' + expos);
        var keyboardhtml =
            //'<div' + ' id="errorContainer' + index + '" style="margin-top:100px;">' + 'Errors: Click here to toggle display' +
            '<div ' + 'id="keyboardobject' + 0 + '" >' +
                '<p'+' id="keyboardPrompt">After selecting a program, place your cursor over a key to view your history.<'+'/p>'+
            '<' + 'object preserveAspectRatio="xMinYmin meet" viewbox="0 0 120 120" ' + ' data="../images/cleanboard.svg" type="image/svg+xml" id="keyboardsvg_e'+index+'" style=" margin:20px; position: relative;"></object' + '>' +
            '<ul' + '>';
            //'<% for (var j = 0; j < exercises[expos].missedKeys.length; j++){%>'+
        for (var j = 0; j < exercise.missedKeys.length; j++)
        {
            var key2 =
                '<div' + ' id="PopUp' + index + '' + exercise.missedKeys[j].missed + '" class="exPopUp"  data-potato="' + exercise.missedKeys[j].errorCount + '"' +
                'style="position: absolute; opacity:0; z-index:0; top:0px;left:0px; border: solid black 2px; padding: 10px; background-color: rgb(200,100,100); text-align: center; font-size: 14px; width: 145px;">';
                //'<% if (exercises[expos].missedKeys[j].errorCount > 0) {%>'+
            keyboardhtml+=key2;
            if (exercise.missedKeys[j].errorCount > 0) {
                var key3 =
                    '<li' + '>Key: ' + exercise.missedKeys[j].missed + '</' + 'li>' +
                    '<li' + '>Error(s): ' + exercise.missedKeys[j].typed + '</'+'li>';
                keyboardhtml += key3;
            }
            else {
                var key4 =
                    '<li' + '>Key: ' + exercise.missedKeys[j].missed + '</'+'li>' +
                    '<li' + '>Error(' + 's): Flawless!!<' + '/li>';
                keyboardhtml += key4;
            }
            //'<%}%>'+
            var key5 =
            '<li' + '>Clean Streak: ' + exercise.missedKeys[j].history + '</'+'li>' +
            '</d' + 'iv>';
            keyboardhtml += key5;
        }
        var key9 =
        '</u'+'l>';
        //'</d'+'iv>';
        keyboardhtml += key9;

        document.getElementById('metricsContainer').innerHTML = keyboardhtml;
        //console.log(keyboardhtml);
    }
    var addMetricsListener = function(index) {

        var keyboard = document.getElementById("keyboardsvg_e"+index);
        if (keyboard)
        {
            keyboard.addEventListener("load", function ()
            {
                var svgDoc = keyboard.contentDocument;
                for (var i = 0; i < LetterIds.length; i++)
                {

                    var tletter = LetterIds[i];
                    var potato = document.getElementById('PopUp' + index + tletter.letter);
                    var rect = svgDoc.getElementById('rect'.concat(tletter.path));
                    if (potato) {
                        potato = potato.getAttribute('data-potato');

                        if (potato > 0)
                            rect.style.fill = '#ffcb19';
                        if (potato > 5)
                            rect.style.fill = '#ff0000';
                        if (potato < 1)
                            rect.style.fill = '#7cd200';
                    }
                        var lastHovered = "";
                    (function (letter, rect)
                    {

                        rect.style.stroke = 'black';
                        rect.style.strokeWidth = '1px';
                        rect.addEventListener('mouseover', keyMouseOver);
                        function keyMouseOver(event)
                        {
                            //console.log("mouseover event target x: "+event.target.x.valueOf() + " y: "+event.target.y.valueOf());
                            var el = document.getElementById('PopUp' + index + ((letter.letter)));
                            console.log(rect.id);
                            if (el) {
                                if (lastHovered != "")
                                {
                                    lastHovered.style.opacity = 0;
                                    lastHovered.style.zIndex = 0;
                                    lastHovered.style.transitionDelay = "0s";
                                    lastHovered.style.visibility = "hidden";
                                }
                                var x = 10 + +rect.getBoundingClientRect().left - (rect.getBoundingClientRect().width);
                                var y = 50 + +rect.getBoundingClientRect().top - (rect.getBoundingClientRect().height);
                                if (letter.letter === 'space')
                                {
                                    x = rect.getBoundingClientRect().left + (rect.getBoundingClientRect().width / 3);
                                }
                                rect.style.stroke = '#48d1cc';
                                rect.style.strokeWidth = '4px';
                                el.style.left = x + "px";
                                //el.style.overflow = true;
                                el.style.top = y + "px";
                                el.style.opacity = 1;
                                el.style.zIndex = 99+(index*index);
                                //el.style.display = "block";
                                el.style.position = "absolute";
                                el.style.transition= "z-index 0.5s ease-out, opacity 0.5s ease-out";
                                el.style.visibility = "visible";
                                //el.style.transitionDelay="0s";
                            }

                        };
                        rect.addEventListener('mouseout', keyMouseOut);
                        function keyMouseOut()
                        {
                            var tel = document.getElementById('PopUp' + index + ((letter.letter)));
                            if (tel)
                            {
                                lastHovered = tel;
                                rect.style.stroke = 'black';
                                rect.style.strokeWidth = '1px';
                                tel.style.opacity=0;
                                tel.style.zIndex=0;
                                tel.style.transitionDelay = "1s";
                                tel.style.visibility = "hidden";
                            }
                        };

                    })(tletter, rect);
                }
            }, false);
        }
    };
    var ids = [

        {letter: "1", path:'4919'},
        {letter: "2", path:'4921'},
        {letter: "3", path:'4923'},
        {letter: "4", path:'4925'},
        {letter: "5", path:'4927'},
        {letter: "6", path:'4929'},
        {letter: "7", path:'4931'},
        {letter: "8", path:'4933'},
        {letter: "9", path:'4935'},
        {letter: "0", path:'4937'},
        {letter: "space", path:'5029'},
        {letter: "q", path:'4947'},
        {letter: "w", path:'4949'},
        {letter: "e", path:'4951'},
        {letter: "r", path:'4953'},
        {letter: "t", path:'4955'},
        {letter: "y", path:'4957'},
        {letter: "u", path:'4959'},
        {letter: "i", path:'4961'},
        {letter: "o", path:'4963'},
        {letter: "p", path:'4965'},
        {letter: "a", path:'4975'},
        {letter: "s", path:'4977'},
        {letter: "d", path:'4979'},
        {letter: "f", path:'4981'},
        {letter: "g", path:'4983'},
        {letter: "h", path:'4985'},
        {letter: "j", path:'4987'},
        {letter: "k", path:'4989'},
        {letter: "l", path:'4991'},
        {letter: "z", path:'5001'},
        {letter: "x", path:'5003'},
        {letter: "c", path:'5005'},
        {letter: "v", path:'5007'},
        {letter: "b", path:'5009'},
        {letter: "n", path:'5011'},
        {letter: "m", path:'5013'}//,
        //{letter: "Q", path:'4947'},
        //{letter: "W", path:'4949'},
        //{letter: "E", path:'4951'},
        //{letter: "R", path:'4953'},
        //{letter: "T", path:'4955'},
        //{letter: "Y", path:'4957'},
        //{letter: "U", path:'4959'},
        //{letter: "I", path:'4961'},
        //{letter: "O", path:'4963'},
        //{letter: "P", path:'4965'},
        //{letter: "A", path:'4975'},
        //{letter: "S", path:'4977'},
        //{letter: "D", path:'4979'},
        //{letter: "F", path:'4981'},
        //{letter: "G", path:'4983'},
        //{letter: "H", path:'4985'},
        //{letter: "J", path:'4987'},
        //{letter: "K", path:'4989'},
        //{letter: "L", path:'4991'},
        //{letter: "Z", path:'5001'},
        //{letter: "X", path:'5003'},
        //{letter: "C", path:'5005'},
        //{letter: "V", path:'5007'},
        //{letter: "B", path:'5009'},
        //{letter: "N", path:'5011'},
        //{letter: "M", path:'5013'}
    ];
    var cids =
        [{letter: "space", path: '2362'},
            {letter: "a", path: '2292'},
            {letter: "b", path: '2336'},
            {letter: "c", path: '2330'},
            {letter: "d", path: '2298'},
            {letter: "e", path: '2256'},
            {letter: "f", path: '2300'},
            {letter: "g", path: '2302'},
            {letter: "h", path: '2306'},
            {letter: "i", path: '2270'},
            {letter: "j", path: '2308'},
            {letter: "k", path: '2312'},
            {letter: "l", path: '2314'},
            {letter: "m", path: '2340'},
            {letter: "n", path: '2338'},
            {letter: "o", path: '2272'},
            {letter: "p", path: '2274'},
            {letter: "q", path: '2252'},
            {letter: "r", path: '2258'},
            {letter: "s", path: '2296'},
            {letter: "t", path: '2262'},
            {letter: "u", path: '2266'},
            {letter: "v", path: '2334'},
            {letter: "w", path: '2254'},
            {letter: "x", path: '2326'},
            {letter: "y", path: '2264'},
            {letter: "z", path: '2324'}
//                        {letter: "A", path:'2292'},
//                        {letter: "B", path:'2336'},
//                        {letter: "C", path:'2330'},
//                        {letter: "D", path:'2298'},
//                        {letter: "E", path:'2256'},
//                        {letter: "F", path:'2300'},
//                        {letter: "G", path:'2302'},
//                        {letter: "H", path:'2306'},
//                        {letter: "I", path:'2270'},
//                        {letter: "J", path:'2308'},
//                        {letter: "K", path:'2312'},
//                        {letter: "L", path:'2314'},
//                        {letter: "M", path:'2340'},
//                        {letter: "N", path:'2338'},
//                        {letter: "O", path:'2272'},
//                        {letter: "P", path:'2274'},
//                        {letter: "Q", path:'2252'},
//                        {letter: "R", path:'2258'},
//                        {letter: "S", path:'2296'},
//                        {letter: "T", path:'2262'},
//                        {letter: "U", path:'2266'},
//                        {letter: "V", path:'2334'},
//                        {letter: "W", path:'2254'},
//                        {letter: "X", path:'2326'},
//                        {letter: "Y", path:'2264'},
//                        {letter: "Z", path:'2324'}
    ];
    window.LetterIds = ids;

    function addMetricsToggle(index)
    {
        (function(index){
            var board = document.getElementById('errorContainer'+index);
            var boardObj = document.getElementById('keyboardobject'+index);
            if (board)
            board.addEventListener('click', function (event) {
                if (boardObj.style.display != 'block')
                {
                    boardObj.style.display = 'block';
                }
                else
                {
                    boardObj.style.display = 'none';
                }
            }, false);
        })(index);

    }
    console.log('selex:'+selex);
    setGraph(exData[0]);
    var spl = selex.split('');
    setKeyboardPrompt(1, 1);
    poop();
    document.getElementById('rategraph').getContext('2d').fillText("No Data Selected", document.getElementById('rategraph').width/3, document.getElementById('rategraph').height/2);
    document.getElementById('rategraph').getContext('2d').font = "48px Arial";
    document.getElementById('timegraph').getContext('2d').fillText("No Data Selected",  document.getElementById('timegraph').width/3, document.getElementById('timegraph').height/2);
    document.getElementById('timegraph').getContext('2d').font = "48px Arial";

};

jqAjax.ajax({
    url: 'getExData',
    type: 'get',
    dataType: 'json',
    success: function(data){
        //alert('success:'+data);
        document.getElementById('exContainer').style.display = "block";
        //new Chart(document.getElementById('recordgraph').getContext('2d'));
        exData = data;
        mymain(); //fix thiss here to make it all hide/show appropriately7

    },
    error: function(xhr,status){
        //alert('error:'+status);
    },
    complete: function(xhr,status){
        //alert('complete:'+status);
    }
});
//makes sure DOM is ready then loads main()
//if( !!(window.addEventListener)) {
//
//
//
//    window.addEventListener ("DOMContentLoaded", main);
//
//}else{ //MSIE
//    window.attachEvent("onload", main);
//}

