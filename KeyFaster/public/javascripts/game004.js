/**
 * Created by Daniel on 5/9/2015.
 */
    var fox = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];
    var text7 = ['w','W','a','A','s','S','d','D','w','a','s','d','W','A','S','D','W','s','W','A','D','S','w','w','W','d'];
    var text8 = ['q','w','e','r','t','d','f','1','2','3','4','5','w','r','e','q'];
    var text9 = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];
var text10 = ['t','h','e','r', 'o','n', 'a','n', 'r','e', 'h','e', 'i','n', 'e','d', 'n','d', 'h','a', 'a','t', 'e','n', 'e','s', 'o','f', 'o','r', 'n','t', 'e','a', 't','i', 't','o', 'i','t', 's','t', 'i','o', 'l','e', 'i','s', 'o','u', 'a','r', 'a','s', 'd','e', 'r','t', 'v','e'];
    var canvas, cardContainer,cardBackground,images;
    var loader = null;
    var whichercise = "4";
    var stage = null;
    var running = false;

    function setText(id) {
        if (!running)
        {
            id = id.value;
            if (id === "1")
                fox = text9;
            else if (id === "2")
                fox = text7;
            else if (id === "3")
                fox = text8;
            else if (id === "4")
                fox = text10;
            whichercise = id;
        }
    }
    (function (window) {
        function Card(textValue) {

            this.initialize();
            this.textValue = textValue;
            this.backUp = true;
            //this.velocity = velocity;
            //this.delay = delay;
        }
        var textValue = "N/A";
        var backUp = true;
        //var tickCounter = 0;
        //Inheritance from Container
        Card.prototype = new createjs.Container();
        Card.prototype.Container_initialize = Card.prototype.initialize;
        Card.prototype.Container_tick = Card.prototype._tick;

        Card.prototype.initialize = function () {
            //call to initialize() method from parent class
            this.Container_initialize();

            //console.log("Letter initialized");
        };
        Card.prototype._tick = function () {
            //call to _tick method from parent class
            this.Container_tick();

            //console.log("Letter Ticked");
        };

        Card.prototype.onKeypress = function (event) {
            //handleInput(event);

            preventDefault(event);
        };
        Card.prototype.update = function () {

        };
        Card.prototype.onclick = function (event) {

            tweenFlip(this, 1, backUp);
            //flip this card over
        };

        window.Card = Card;
    }(window));
    function handleInput(event)
    {
        var keypressed = '[N/A]';
        //save the input pressed for the update function to capture and act upon
        keypressed = getChar(event);
        if (keypressed === " ")
            keypressed = "space";
        pressedKeys.push(keypressed);
        lastKeyPressed = keypressed;
        //console.log('pressed:: '+keypressed);
        //timerDisplay.x -= 5;
        //timerDisplay.y -= 5;
        return keypressed;
    }
    function getChar(event) {

        if (event.which == null) {
            return String.fromCharCode(event.keyCode); // IE
        } else if (event.which != 0 && event.charCode != 0) {
            //console.log(String.fromCharCode(event.which));
            return String.fromCharCode(event.which);  // the rest
        } else {
            return null; // special key
        }
    }
    function preventDefault(e)
    {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }
    function runGameTimer()
    {
        if (running) {
            frameCount += 1;
            if (frameCount % (FPS / 10) === 0) {
                gameTimer = frameCount / (FPS);
                timerDisplay.text = "Time Elapsed: " + gameTimer;
            }
        }
    }
    function post(path, params, method) {
        method = method || "post";
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }
    function tweenComplete(tween)
    {
        tween.destroy();
    }
    function tweenObj(myObject, desX, desY) {
        myTween = createjs.Tween.get(myObject,{loop:false, override:true})
            .wait(0)
            .to({x:desX, y:desY, rotation:0}, 500)
            .call(tweenComplete);

    }
    function tweenLetter(myObject, desX, desY, velocity)
    {
        var letterTween = createjs.Tween.get(myObject, {loop:false})
            .wait(0)
            .to({x:desX, y:desY, rotation:0}, (+velocity*230))
            .call(tweenComplete);
    }
    function tweenFade(myObject, ttl)
    {
        var fadeTween = createjs.Tween.get(myObject,{loop:false, override:true})
            .wait(+ttl/2)
            .to({alpha:0},(+ttl/2))
            .call(tweenComplete);
    }
    function tweenFlip(myObject, speed, backUp)
    {
        var xArc = 180;
        var yArc = 180;
        if (backUp){
            var tFlip = TweenLite.to(myObject, 1, {scaleX:0.01, z:-300, oncomplete: flipComplete});
            myObject.backUp = false;
            //var tReturn = TweenLite.to(myObject, 1, {scaleX:1, z:0})
        }
        else
        {
            var tmax = TweenLite.to(myObject, 1, {scaleX:1, z:0})
        }
        function flipComplete(event)
        {
            var tmax = TweenLite.to(myObject, 1, {delay:1,scaleX:1, z:0})
        }
        //var tmax = TweenMax.to(myObject, 1, {x: 500, y:500, transformPerspective:200, rotateX:360});

            console.log('flipping after clicking '+ this.x + ', '+this.y);
    }
    function setupCanvas()
    {
        canvas = document.getElementById('canvas');
        canvas.width = 800;
        canvas.height = 600;
        stage = new createjs.Stage(canvas);
        cardContainer = new createjs.Container();
        var background = new createjs.Shape();
        background.graphics.beginFill('#96db32').drawRect(0, 0, canvas.width, canvas.height);
        background.x = 0;
        background.y = 0;
        stage.addChild(background);
        stage.addChild(cardContainer);
        //var cw = (800/5);
        for (var i = 1; i <= 10; i++)
        {
            var card1 = new Card(fox[i]);
            card1.on('click', card1.onclick, false);
            var cw = 160;
            card1.width = cw;
            card1.height = 250;


            if (i <= 5){
                card1.x = (cw * i) - (cw/2);
                card1.y = 125;
            }

            if (i > 5){
                card1.x = (cw * (i-5)) - (cw/2);
                card1.y = 375;
            }

            //new createjs.Shape();
            //cardBackground.graphics.beginFill('#00'+(i-1)+''+(i-1)+'ff').drawRect(0, 0, card1.width, card1.height);

            //cardBackground.width = card1.width;
            //cardBackground.height = card1.height;
            cardBackground = new createjs.Bitmap(loader.getResult("cardback"));
            cardBackground.x = 0;
            cardBackground.y = 0;
            cardBackground.scaleX = cw/cardBackground.getBounds().width;
            cardBackground.scaleY = 250/cardBackground.getBounds().height;

            card1.addChild(cardBackground);
            card1.regX = card1.width/2;
            card1.regY = card1.height/2;
            cardBackground.regX = cardBackground.width/2;
            cardBackground.regY = cardBackground.height/2;

            cardContainer.addChild(card1);
            TweenLite.ticker.addEventListener("tick", stage.update, stage);
            //stage.update();
        }


    }
    function addNewCard(index)
    {





        //add card back image, and card frontside

    }
function handleFileLoad(o) {
    //You could store all your images in object to call them easily.
    //cardBackground = new createjs.Bitmap(o.result);
    console.log('loaded file:' + o.verbose);
    if (o.type == "image") {
        images[o.id] = o.item;
    }
}

function handleComplete(event) {
    cardBackground = new createjs.Bitmap(loader.getResult("cardback"));
    //OR samething
    //var bg = new createjs.Bitmap(images['image']);
    //stage.addChild(cardContainer);
    main();
}

function init()
{
    images = images || {};
    var manifest = {
        src: '../images/cardback.png',
        id: 'cardback'
    }
    loader = new createjs.LoadQueue(true);
    //loader.on('fileload', handleFileLoad, this);
    loader.on('complete', handleComplete, this);
    loader.loadFile(manifest);
    loader.load();
}
    function main() {


        setupCanvas(); //sets up the canvas
        createjs.EventDispatcher.initialize(Card.prototype);
        //console.log('initialized LetterDispatcher');
        createjs.Ticker.on("tick", loop);
        createjs.Ticker.setFPS(30);
        document.addEventListener('keypress', handleInput);
        onResize();
        var el = document.getElementById('trainerSelector');
        //el.value = "1";
        el.selectedIndex = 3;
        el.scrollIntoView(true);
        el.focus();



    }
    function loop(event)
    {
        if (!event.paused)
        {
            if (running)
            {
                runGameTimer();
                //stage.update();
            }
            //letter1.position.x = 0;
            stage.update();
        }
    }


    if (!!window.addEventListener){

        window.addEventListener("DOMContentLoaded", init, false);
    } else {
        window.attachEvent('onload', main);
    }