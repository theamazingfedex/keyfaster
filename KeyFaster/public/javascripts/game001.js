
var words = ['the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'];
var fox = ['t', 'h', 'e', ' ', 'q', 'u', 'i', 'c', 'k', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'f', 'o', 'x', ' ', 'j', 'u', 'm', 'p', 's', ' ', 'o', 'v', 'e', 'r', ' ', 'a', ' ', 'l', 'a', 'z', 'y', ' ', 'd', 'o', 'g'];
var stage, circle, myText, canvas, startBtn, startTxt, invalidkey, timerDisplay, canvastime, cColor, endBtn, splatContainer;
var canvast = 0;
var foxCon = 0;
var whichercise = "1";
var tumbling = false;

var text7 = ['w','W','a','A','s','S','d','D','w','a','s','d','W','A','S','D','W','s','W','A','D','S','w','w','W','d'];
var text8 = ['q','w','e','r','t','d','f','1','2','3','4','5','w','r','e','q'];
var text9 = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];

function tweenFade(myObject, ttl) {
    var fadeTween = createjs.Tween.get(myObject, {loop: false, override: true})
        .wait(+ttl / 2)
        .to({alpha: 0}, (+ttl / 2))
        .call(completedTween(myObject));
}
function showEndGame(rate, exId)
{
    tumbling = false;
    running = false;
    canvas.onclick = function () {
        //boop
    };
    endBtn = new createjs.Container();

    var btnBackgroundEnd = new createjs.Shape();
    btnBackgroundEnd.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
    btnBackgroundEnd.y = 300;//stage.getBounds().height/2 ;
    btnBackgroundEnd.x = 400;//stage.getBounds().width/2;
    btnBackgroundEnd.width = 800;// stage.getBounds().width;
    btnBackgroundEnd.height = 300;//stage.getBounds().height/2;
    btnBackgroundEnd.regX = btnBackgroundEnd.width/2;
    btnBackgroundEnd.regY = btnBackgroundEnd.height/2;
    //btnBackgroundEnd.x = canvas.width/2;
    endBtn.addChild(btnBackgroundEnd);
    stage.update();
    var exNameEnd = new createjs.Text('SplatterKeys Results', '36px Irish Grover', '#000');
    exNameEnd.x = btnBackgroundEnd.width/2;
    exNameEnd.y = btnBackgroundEnd.y - 100;
    exNameEnd.regX = exNameEnd.getBounds().width/2;
    exNameEnd.regY = exNameEnd.getBounds().height/2;
    endBtn.addChild(exNameEnd);
    stage.update();
    var missedKeyList = missedKeys.keylist;
    var missedKeyString = '';
    for ( var i = 0; i < missedKeyList.length; i++)
    {
        if (typeof missedKeyList[i].missed != 'undefined' || missedKeyList[i].missed != '') {
            missedKeyString += missedKeyList[i].missed;
            if (i != missedKeyList.length-1)
                missedKeyString+=', ';
            else {
                if (missedKeyString == '')
                    missedKeyString += 'None; Perfect Score!';
            }
            if ((i+1) % 6 == 0)
                missedKeyString += '\n               ';
        }
    }
    //missedKeyString = missedKeyString.split(',');
    //missedKeyString[Math.floor(missedKeys.keylist.length/2)] += ' ';
    //if (missedKeys.keylist.length > 4)

    var exDescriptionEnd = new createjs.Text(
        '\nWPM: '+rate + '\n'+
        'Completion Time: '+canvast+'\n'+
        'Training Program: '+getSelectionText()+'\n'
        ,
        "20px Architects Daughter", '#000');

    exDescriptionEnd.lineWidth = 600;
    //exDescriptionEnd.maxWidth = 600;
    exDescriptionEnd.x = exNameEnd.x;
    exDescriptionEnd.y = exNameEnd.y + 20;
    exDescriptionEnd.regX = exDescriptionEnd.getBounds().width/2;
    endBtn.addChild(exDescriptionEnd);
    stage.update();
    var missedKeyResults = new createjs.Text('Missed Keys: '+ missedKeyString, "24px Architects Daughter", '#ff0000');
    missedKeyResults.lineWidth = 600;
    //missedKeyResults.maxWidth = 600;
    missedKeyResults.x = exNameEnd.x - 20;
    missedKeyResults.y = exNameEnd.y + 100;
    missedKeyResults.regX = exDescriptionEnd.getBounds().width/2;
    endBtn.addChild(missedKeyResults);
    stage.update();
    var exCharbackEnd = new createjs.Shape();
    exCharbackEnd.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
    exCharbackEnd.y = btnBackgroundEnd.y + 110;
    endBtn.addChild(exCharbackEnd);
    stage.update();
    var end = new createjs.Text('Press ENTER to try again', '36px Irish Grover', "#000000");
    end.className = "hoverable";
    end.x = exDescriptionEnd.x;
    end.y = exCharbackEnd.y;
    end.regX = end.getBounds().width/2;
    //end.regY = end.getBounds().height/2;
    endBtn.addChild(end);
    stage.addChild(endBtn);
    //onResize();
    stage.update();
    exCharbackEnd.on('click', startGame);
    exCharbackEnd.on('mouseover', function (e) {
        end.color = "#0000ff";
        stage.update();
    });
    exCharbackEnd.on('mouseout', function (e) {
        end.color = "#000000";
        stage.update();
    });
    var recordsCharback = new createjs.Shape();
    recordsCharback.graphics.beginFill("#EB7F00").drawRect(0,0,800,50);
    recordsCharback.y = btnBackgroundEnd.y + 160;
    endBtn.addChild(recordsCharback);
    stage.update();
    var recordsText = new createjs.Text('Click Here to Browse Your Records', '36px Irish Grover', "#000000");
    recordsText.className = "hoverable";
    recordsText.x = exDescriptionEnd.x;//(stage.getBounds().width/2) - recordsText.getBounds().width/2;
    recordsText.y = recordsCharback.y;
    recordsText.regX = recordsText.getBounds().width/2;
    endBtn.addChild(recordsText);
    stage.update();

    recordsCharback.on('click', function () {
        window.location = '/dashboard';
    });
    recordsCharback.on('mouseover', function (e) {
        recordsText.color = "#0000ff";
        stage.update();
    });
    recordsCharback.on('mouseout', function (e) {
        recordsText.color = "#000000";
        stage.update();
    });
    //console.log('MissedKeys::: '+JSON.stringify(missedKeys.keylist));
    //data = JSON.stringify(data);
    //missedKeys.k
    //console.log(data);
    var exData =
    {
        exercise: (+(6 * (exId)) + (whichercise.valueOf() - 1)).toString(),
            completionTime: canvast,
        rate: rate,
        missedKeys: missedKeys.keylist
    };
    exData = JSON.stringify(exData);
    console.log((new Date().getTime())+'\n'+exData);
    jqAjax.ajax({
        url: 'submit',
        type: 'post',
        data:{
            exData: exData
        },
        success: function(data){
            hideSelect(tumbling);
            console.log('Saved exercise data: '+(new Date().getTime())+'\n'+exData);
        },
        error: function(xhr,status){
            alert('Error submitting exercise data: '+status);
        },
        complete: function(xhr,status){
            //alert('complete:'+status);
        }
    });

    //post('/exercises/submit', );

}
var missedKeys = {
    keylist: [],
    add: function (missed, typed) {
        this.keylist[this.keylist.length] = {missed:missed, typed:typed};//{missed:missed, typed:typed};
    },
    get: function (index) {
        return this.keylist[index];
    },
    clear: function () {
        this.keylist = [];
    },
    printable: function () {
        var missedString = 'Expected|Typed  [';
        for (var i = 0; i < this.keylist.length; i++)
        {
            missedString += this.keylist[i].missed + '|' + this.keylist[i].typed;
            if (i != this.keylist.length -1)
            {
                missedString += ', ';
            }
        }
        missedString += ']';
        return missedString;
    }

};

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    //alert(form);
    form.submit();
}
function secondize(n)
{
    if (n!=0)
    {
        n= n / 1000;
        n = (Math.floor(10*n)/10);
    }
    return n;
}

var timer = {
    totalTime: 0,
    init: function() {
        this.startTime = (new Date()).getTime();
        return this;
    },
    stop: function() {
        // returns time difference (in seconds: getSeconds()) between start time and time that this function was called

        var difference = new Date();
        var timeUsed=secondize(difference.setTime((new Date().getTime()) - this.startTime));
        totalTime = timeUsed;

        return totalTime;
    },
    getUpdate: function(){
        var difference = new Date();
        var timeElapsed = secondize(difference.setTime((new Date().getTime()) - this.startTime));
        return timeElapsed;
    }
};
function completedTween(myObject)
{
    stage.removeChild(myObject);
    return tweenComplete;
}
function updateEx1()
{
    runGameTimer();
    if (timerDisplay)
        timerDisplay.text = 'Time Elapsed: ' + gameTimer + ' seconds ';
    stage.update();
}
function main() {
    setText({value:tpid});


    setupCanvas();
    showStartBtn();
    console.log('User missedkeys: ' + missedKeys.printable());

    //alert('boobies');
    //console.log('boobies');
    createjs.Ticker.on("tick", updateEx1);
    createjs.Ticker.setFPS(30);
    canvas.onclick = startGame;
    //document.addEventListener('keydown', function (e) {
    //});
    document.addEventListener('keypress', function (event) {
        //console.log('got char:: ' + getChar(event));
        if (!tumbling && event.keyCode == 13) {
            pressedKeys.shift();

            startGame();
        }
        else {
            (function (event, fox) {
                getNextLetter(getChar(event), fox);
                preventDefault(event);

            })(event, fox);
        }
    });
    onResize();
    var el = document.getElementById('trainerSelector');
    //console.log("usertpid: "+user.tpid);
    //setText({value:user.tpid});
    el.selectedIndex = +tpid-1;
    //el.scrollIntoView(true);
    el.focus();

};
function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function showStartBtn()
{
    var r = 255;//genCol();
    var g = 255;//genCol();
    var b = 255;//genCol();


    startBtn = new createjs.Container();

    var btnBackground = new createjs.Shape();
    btnBackground.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
    btnBackground.width = 800;
    btnBackground.height = 300;
    btnBackground.regX = 400;
    btnBackground.regY = 150;
    btnBackground.x = canvas.width/2;
    btnBackground.y = canvas.height/2;
    startBtn.addChild(btnBackground);
    stage.update();
    var exName = new createjs.Text('SplatterKeys', '36px Irish Grover', isWhiteText(r,g,b,1));
    exName.x = canvas.width/2;
    exName.y = canvas.height/2 - 100;
    exName.regX = exName.getBounds().width/2;
    exName.regY = exName.getBounds().height/2;
    startBtn.addChild(exName);
    stage.update();
    var exDescription = new createjs.Text(
        'Tips for playing:\n'+
        '1. Place your fingers on the home row of your keyboard.\n' +
        '2. Press the displayed key as soon as it shows up.\n' +
        '3. Get a perfect score on the same program 5 times in a row to\n' +
        '   reset your error history for that specific training program.',
        "20px Architects Daughter", isWhiteText(r,g,b,1));
    exDescription.x = exName.x - exDescription.getBounds().width/2;
    exDescription.y = exName.y + 20;
    startBtn.addChild(exDescription);
    stage.update();

    var exCharback = new createjs.Shape();
    exCharback.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
    exCharback.y = btnBackground.y + 100;
    startBtn.addChild(exCharback);
    stage.update();
    var startGame = new createjs.Text('Press ENTER to Begin', '36px Irish Grover', "#000000");
    startGame.className = "hoverable";
    startGame.x = exName.x - startGame.getBounds().width/2;
    startGame.y = exCharback.y;
    startBtn.addChild(startGame);
    stage.update();

    exCharback.on('mouseover', function (e) {
        startGame.color = "#0000ff";
        stage.update();
    });
    exCharback.on('mouseout', function (e) {
        startGame.color = "#000000";
        stage.update();
    });

    //stage.addChild(startBtn);
    //startTxt = new createjs.Text('Click to Begin', "24px Arial", isWhiteText(r,g,b,0.9));
    //stage.addChild(startTxt);
    stage.addChild(startBtn);

}
function startGame() {
    //canvas.removeEventListener('click', startGame, false);
    if (!tumbling)
    {
        if (timerDisplay)
            stage.removeChild(timerDisplay);
        if (endBtn)
            stage.removeChild(endBtn);
        if (startBtn)
            stage.removeChild(startBtn);
        if (splatContainer)
            stage.removeChild(splatContainer);
        splatContainer = new createjs.Container();
        stage.addChild(splatContainer);
        invalidkey = false;
        missedKeys.clear();
        canvastime = timer.init();
        stage.removeChild(startBtn, startTxt, timerDisplay, endBtn);
        timerDisplay = new createjs.Text('Time Elapsed: ' + canvastime.getUpdate() +' seconds ', "24px Arial", '#fff');
        timerDisplay.x = 50;
        timerDisplay.y = 0;
        stage.addChild(timerDisplay);
        stage.update();
        //display_c(3600);
        tumbling =  true;
        foxCon = 0;
        placeLetterRandomly(fox[foxCon]);
        hideSelect(tumbling);
    }
};
function genCol()
{
    return 50+Math.floor(Math.random()*155);
}
function getChar(event) {
    if (event.which == null) {
        return String.fromCharCode(event.keyCode); // IE
    } else if (event.which != 0 && event.charCode != 0) {
        //console.log(String.fromCharCode(event.which));
        return String.fromCharCode(event.which);  // the rest
    } else {
        return null; // special key
    }
};

function addLetterToCanvas(left, top, ch) {
    circle = new createjs.Shape();
    var r = genCol();
    var g = genCol();
    var b = genCol();
    //console.log('color: r('+r+')  g('+g+')  b('+b+')');
    cColor = {r:r, g:g, b:b, o:1};
    circle.graphics.beginFill(createjs.Graphics.getRGB(r, g, b,0.9)).drawCircle(0, 0, 30);
    circle.x = left;
    circle.y = top;
    myText = new createjs.Text(ch, "24px Arial", isWhiteText(r, g, b,0.9));
    myText.x = left;
    myText.y = top;
    myText.regX = myText.getBounds().width/2;
    myText.regY = myText.getBounds().height/2;
    stage.addChild(circle);
    stage.addChild(myText);
    stage.update();
}

function isWhiteText(R,G,B, gamma)
{
    R = R / 255;
    G = G / 255;
    B = B / 255;
    gamma = 2.2;
    var L = 0.2126 * Math.pow( R, gamma )
        + 0.7152 * Math.pow( G, gamma )
        + 0.0722 * Math.pow( B, gamma );

    var using_black = ( L > 0.4 );
    if (using_black) {
        return '#000000';
    }
    else if (!using_black){
        return '#ffffff';
    }
}
function randomColor() {
    var color = '';

    var values = ['a', 'b', 'c', 'd', 'e', '2', '3', '4', '5', '6', '7', '8'];
    for (c = 0; c < 6; c++) {
        no = Math.floor(Math.random() * 14);
        color += values[no];
    }
    return '#' + color;

};


function getNextLetter(inputChar, fox)
{
    if (!tumbling)
        return null;



    if (foxCon > fox.length) { //player has reached the end of the exercise

    }
    else if (inputChar === fox[foxCon])
    {   //player netered the correct key and is at the end of the exercise
        invalidkey = false;
        removeClearedLetter();
        foxCon += 1;
        if (foxCon === fox.length)
        {  //player entered the correct
            //stop the timer
            tumbling = false;
            canvast = canvastime.stop();
            var rate = 9/(canvast/60);
            rate = Math.floor(rate*100)/100;
            timerDisplay.text = 'Time Elapsed: ' + canvast + ' seconds; ' + rate + ' wpm';//.getUpdate();
            //stage.update();
            //console.log('----finished the exercise in: ' + canvast + ' seconds with a WPM of: '+rate);

            canvast = (Math.floor(canvast*100))/100;
            //alert('missedkeys: ' + JSON.stringify(missedKeys.keylist));
            //if (whichercise === "1")
            running = false;
            tumbling = false;
            showEndGame(rate, 0 );
                //if (whichercise === "2")
            //    post('/exercises/two', {exercise: whichercise, completionTime: canvast, rate: rate, missedKeys: JSON.stringify(missedKeys.keylist)});
            //if (whichercise === "3")
            //    post('/exercises/three', {exercise: whichercise, completionTime: canvast, rate: rate, missedKeys: JSON.stringify(missedKeys.keylist)});
            //if (whichercise === "4")
            //    post('/exercises/four', {exercise: whichercise, completionTime: canvast, rate: rate, missedKeys: JSON.stringify(missedKeys.keylist)});
            //post('/exercises/one', {exercise: whichercise, completionTime: canvast, rate: rate, missedKeys: JSON.stringify(missedKeys.keylist)});
            //tumbling = false;
            //showStartBtn();
        }
        else
        {  //player entered the correct key and keeps going

            placeLetterRandomly(fox[foxCon]);
            return fox[foxCon]
        }

    }
    else { //player entered the wrong key
        createjs.Sound.play("error.wav");
        if (!invalidkey) {
                invalidkey = true;
            var tempCon = fox[foxCon];
            if (inputChar === ' ')
                inputChar = 'space';
            if (tempCon === ' ')
                tempCon = 'space';
            missedKeys.add(tempCon, inputChar);
            console.log('expecting ' + tempCon + ' but got ' + inputChar + '; fox index: ' + foxCon);
        }
    }
}

function removeClearedLetter()
{
    var x = circle.x;
    var y = circle.y;
    var splat = new createjs.Bitmap(loader.getResult("paintsplat.png"));
    splat.x = x;
    splat.y = y;
    splat.scaleX = 90/splat.getBounds().width;
    splat.scaleY = 90/splat.getBounds().height;
    splat.regX = splat.getBounds().width/2;
    splat.regY = splat.getBounds().height/2;
    stage.removeChild(circle, myText);
    createjs.Sound.play("splat.wav");
    splatContainer.addChild(splat);
    splat.filters = [new createjs.ColorFilter(cColor.r/205,cColor.g/205,cColor.b/205,cColor.o)];
    splat.cache(0,0, 500, 500);
    stage.update();
    //tweenFade(splat, 3000);
    //for (var i = 0; i < letterChildren.length; i++)
    //{
    //    canvas.removeChild(letterChildren[i]);
    //}
}
function placeLetterRandomly(letter)
{
    if (letter === ' ')
        letter = 'space';
    //var k = Math.floor(Math.random() * ( 90 - 65 + 1 )) + 65;
    //var ch = String.fromCharCode(k);
    var r1 = Math.random();
    var r2 = Math.random();
    var left = Math.floor(80 + (r1 * 640));
    var top = Math.floor(80 + (r2 * 440));
    //console.log('top: ' + top + '   left: ' + left);
    addLetterToCanvas(left, top, letter);
};

function setupCanvas()
{
    canvas = document.getElementById("canvas");

    if (typeof canvas === 'undefined') {
        return;
    } else {
        var context = canvas.getContext('2d');
        canvas.width = 800;
        canvas.height = 600;
        var w = canvas.width;
        var h = canvas.height;
        stage = new createjs.Stage(canvas);

        stage.enableMouseOver();

        //var myText = new createjs.Text("Hello World", "12px Arial", "#ffffff");  //creates text object
        //myText.x = 10; //positions the text
        //myText.y = 50;
        //textStage.addChild(myText);  //adds the text object to the stage
        //
        ////stage.update();  //updates the stage so the text is visible
        //
        //var circle = new createjs.Shape();  //creates object to hold a shape
        //circle.graphics.beginFill("#A66").drawCircle(400, 300, 40);  //creates circle at 0,0, with radius of 40
        //circle.x = circle.y = 50;  //this just sets the x and y equal to 50
        //circle.on("click", function(event){
        //
        //});
        //stage.addChild(circle);  //add the circle to the stage but it is not apparent until the stage is updated

        var background = new createjs.Shape();
        background.graphics.beginFill('#333').drawRect(0, 0, w, h);
        background.x = 0;
        background.y = 0;
        stage.addChild(background);
        stage.update();  //updates the stage
    }
}



//makes sure DOM is ready then loads main()
if( !!(window.addEventListener)) {
    window.addEventListener ("DOMContentLoaded", init(["../images/paintsplat.png","../sounds/splat.wav","../sounds/error.wav"]), false);
}else{ //MSIE
    window.attachEvent("onload", init(["../images/paintsplat.png","../sounds/splat.wav","../sounds/error.wav"]));
}
