/**
 * Created by Daniel on 5/12/2015.
 */
var letterContainer,startBtn,scoreDisplay,hitbar,tpid,endBtn,missedKeys = [], hiddenscr = 0, runningCount = 0, running = false, pressedKeys = [], baseVelocity = 5, velocity = baseVelocity;
function startGame()
{
    //make the current chars fly across the screen at the current speed, and fade out after the user presses start
    if (startBtn)
        stage.removeChild(startBtn);
    if (endBtn)
        stage.removeChild(endBtn);
    document.addEventListener("keydown", dokeydown, true);
    document.addEventListener("keyup", dokeyup, true);
    velocity = baseVelocity;
    if (!running) {
        running = true;
        resetGameTimer();
        hideSelect(true);



        //add the hitbar
        addLettersToContainer();

        stage.addChild(letterContainer);

    }
    else if (running)
    {
        //hideSelect(false);
        //runningCount++;
        //running = false;
        //if (letterContainer) {
        //    //document.body.removeEventListener("updategame",updateEvent, true);
        //    document.body.removeEventListener("updategame", updateEvent, true);
        //    document.body.removeEventListener("keydown", dokeydown, true);
        //    document.body.removeEventListener("keypress", processInput, true);
        //    stage.removeChild(letterContainer);
        //
        //}
        //stage.removeChild(letterContainer);
        //for (var i = 0; i < letterContainer.numChildren; i++)
        //    letterContainer.removeChild(letterContainer.getChildAt(i));
        //letterContainer = null;

        //document.body.removeEventListener("updategame", function (err, event) {
        //
        //});

    }

}
function showStartBtn()
{
    var r = 255;//genCol();
    var g = 255;//genCol();
    var b = 255;//genCol();


    startBtn = new createjs.Container();

    var btnBackground = new createjs.Shape();
    btnBackground.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
    btnBackground.width = 800;
    btnBackground.height = 300;
    btnBackground.regX = 400;
    btnBackground.regY = 150;
    btnBackground.x = canvas.width/2;
    btnBackground.y = canvas.height/2;
    startBtn.addChild(btnBackground);
    stage.update();
    var exName = new createjs.Text('Keyboard Hero', '36px Irish Grover', '#000000');
    exName.x = canvas.width/2;
    exName.y = canvas.height/2 - 100;
    exName.regX = exName.getBounds().width/2;
    exName.regY = exName.getBounds().height/2;
    startBtn.addChild(exName);
    stage.update();
    var exDescription = new createjs.Text(
        'Tips for playing:\n'+
        '1. Place your fingers on the home row of your keyboard.\n' +
        '2. Wait for the letter as it flies across the screen.\n' +
        '3. Press and hold "SPACEBAR" to activate hyper mode.\n' +
        '4. Press the correct key when the letter passes the hit-bar to score!\n' +
        '5. Get a perfect score on the same program 5 times in a row to\n' +
        '   reset your error history for that specific training program.',
        "18px Architects Daughter", '#000000');
    exDescription.x = exName.x - exDescription.getBounds().width/2;
    exDescription.y = exName.y + 20;
    startBtn.addChild(exDescription);
    stage.update();

    var exCharback = new createjs.Shape();
    exCharback.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
    exCharback.y = btnBackground.y + 100;
    startBtn.addChild(exCharback);
    stage.update();
    var startGame = new createjs.Text('Click or Press ENTER to Begin', '36px Irish Grover', "#000000");
    startGame.className = "hoverable";
    startGame.x = exName.x - startGame.getBounds().width/2;
    startGame.y = exCharback.y;
    startBtn.addChild(startGame);
    stage.update();

    exCharback.on('mouseover', function (e) {
        startGame.color = "#0000ff";
        stage.update();
    });
    exCharback.on('mouseout', function (e) {
        startGame.color = "#000000";
        stage.update();
    });
    startBtn.zIndex = 999;
    //stage.addChild(startBtn);
    //startTxt = new createjs.Text('Click to Begin', "24px Arial", isWhiteText(r,g,b,0.9));
    //stage.addChild(startTxt);
    stage.addChild(startBtn);

}
function addLettersToContainer()
{
    var charWidth = 70;
    if (letterContainer) {
        //document.body.removeEventListener("updategame",updateEvent, true);
        //document.body.removeEventListener("updategame", updateEvent, true);
        //letterContainer.onkeypress = null;
        stage.removeChild(letterContainer);

    }
    letterContainer = new createjs.Container();
    var differential = 50;
    for (var i = 1; i < fox.length+1; i++) {

        var myLetter = new createjs.Text('?', "48px Monaco", "#FFF");
        myLetter.x = 800 + (+differential*(i-1)) +(charWidth * (i-1));
        if (i%canter === 1) {
            myLetter.x += differential;
            //differential += 30;
        }
        myLetter.y = 100 + (Math.floor(Math.random()*400));// + (25*runningCount);
        myLetter.regX = myLetter.getBounds().width / 2;
        myLetter.regY = myLetter.getBounds().height / 2;
        var doneOne = false;
        (function(myLetter, i, doneOne){

            letterContainer.addEventListener("updategame", updateGameHandler(myLetter, i, doneOne));

        })(myLetter,i, doneOne);
        //myLetter.update = function(){
        //
        //
        //}


        //myLetter.addChild(myText);
        letterContainer.addChild(myLetter);
    };
}
function updateGameHandler(myLetter, i, doneOne) {
    return function (e) {
        myLetter.text = fox[i-1];
        //console.log('updating my letter: '+myLetter.x);
        myLetter.x -= velocity;
        //pressedKeys = [];
        if (pressedKeys[0] === 'space') {
            pressedKeys.shift();
            return;
        }
        else if ((!doneOne) && (myLetter.x < (+hitbar.x + 45)) && (myLetter.x > (+hitbar.x - 5)) && pressedKeys[0] && (pressedKeys[0] === myLetter.text)) {
            doneOne = true;
            //console.log('checking collision');
            hiddenscr += 5;
            addFilterTo(myLetter, true, i, fox.length);
            //scoreDisplay.getChildAt(2).text = "Score: " + hiddenscr;
            pressedKeys.shift();

        }
        else if (!doneOne && ((myLetter.x < (+hitbar.x + 45)) && (myLetter.x > (+hitbar.x - 5)) && pressedKeys[0] )
            || ((myLetter.x < (+hitbar.x - 5)) && myLetter.color != "#000000"))
        {
            if ((pressedKeys[0] != myLetter.text) && (myLetter.x < (+hitbar.x - 5)))
            {
                doneOne = true;
                hiddenscr -= 4;
                //scoreDisplay.getChildAt(2).text = "Score: " + hiddenscr;
                addFilterTo(myLetter, false, i, fox.length);
                var typedkey = pressedKeys.shift();
                if (!typedkey)
                    typedkey = 'missed';
                missedKeys.push({missed: myLetter.text, typed: typedkey});

            }
            //if (myLetter.color === 0xFFFFFF) {
            //}
        }
        else if (myLetter.x < (+hitbar.x - 5))
        {

        }
        //
    }
};
function showEndGame(rate, exId, missedKeyss)
{
    tumbling = false;
    running = false;

    canvas.onclick = function ()
    {
        //boop
    };
    endBtn = new createjs.Container();

    var btnBackgroundEnd = new createjs.Shape();
    btnBackgroundEnd.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
    btnBackgroundEnd.x = 400;//stage.getBounds().width/2;
    btnBackgroundEnd.y = 300;//stage.getBounds().height/2;
    btnBackgroundEnd.width = 800;//stage.getBounds().width;
    btnBackgroundEnd.height = 300;//stage.getBounds().height/2;
    btnBackgroundEnd.regX = btnBackgroundEnd.width/2;
    btnBackgroundEnd.regY = btnBackgroundEnd.height/2;
    endBtn.addChild(btnBackgroundEnd);
    stage.update();
    var exNameEnd = new createjs.Text('Keyboard Hero Results', '36px Irish Grover', '#000');
    exNameEnd.x = btnBackgroundEnd.width/2;
    exNameEnd.y = btnBackgroundEnd.y - 100;
    exNameEnd.regX = exNameEnd.getBounds().width/2;
    exNameEnd.regY = exNameEnd.getBounds().height/2;
    endBtn.addChild(exNameEnd);
    stage.update();
    var missedKeyList = missedKeyss;
    var missedKeyString = '';
    for ( var i = 0; i < missedKeyList.length; i++)
    {
        if (typeof missedKeyList[i].missed != 'undefined' || missedKeyList[i].missed != '') {
            missedKeyString += missedKeyList[i].missed;
            if (i != missedKeyList.length-1)
                missedKeyString+=', ';
            else {
                if (missedKeyString === '')
                    missedKeyString += 'None; Perfect Score!';
            }
            if ((i+1) % 6 == 0)
                missedKeyString += '\n               ';
        }
    }
    //missedKeyString = missedKeyString.split(',');
    //missedKeyString[Math.floor(missedKeys.keylist.length/2)] += ' ';
    //if (missedKeys.keylist.length > 4)
    if (missedKeyString == '')
        missedKeyString = 'None; Perfect Score!';
    var exDescriptionEnd = new createjs.Text(
        '\nWPM: '+rate + '\n'+
        'Completion Time: '+gameTimer+'\n'+
        'Training Program: '+getSelectionText()+'\n'
        ,
        "20px Architects Daughter", '#000');

    exDescriptionEnd.lineWidth = 600;
    //exDescriptionEnd.maxWidth = 600;
    exDescriptionEnd.x = exNameEnd.x;
    exDescriptionEnd.y = exNameEnd.y + 20;
    exDescriptionEnd.regX = exDescriptionEnd.getBounds().width/2;
    endBtn.addChild(exDescriptionEnd);
    stage.update();
    if (missedKeyString)
    var missedKeyResults = new createjs.Text('Missed Keys: '+ missedKeyString, "24px Architects Daughter", '#ff0000');
    missedKeyResults.lineWidth = 600;
    //missedKeyResults.maxWidth = 600;
    missedKeyResults.x = exNameEnd.x - 20;
    missedKeyResults.y = exNameEnd.y + 100;
    missedKeyResults.regX = exDescriptionEnd.getBounds().width/2;
    endBtn.addChild(missedKeyResults);
    stage.update();

    var exCharbackEnd = new createjs.Shape();
    exCharbackEnd.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
    exCharbackEnd.y = btnBackgroundEnd.y + 110;
    endBtn.addChild(exCharbackEnd);
    stage.update();
    var end = new createjs.Text('Press ENTER to try again', '36px Irish Grover', "#000000");
    end.className = "hoverable";
    end.x = exDescriptionEnd.x;
    end.y = exCharbackEnd.y;
    end.regX = end.getBounds().width/2;
    //end.regY = end.getBounds().height/2;
    endBtn.addChild(end);
    stage.addChild(endBtn);
    onResize();
    stage.update();
    exCharbackEnd.on('click', startGame);
    exCharbackEnd.on('mouseover', function (e) {
        end.color = "#0000ff";
        stage.update();
    });
    exCharbackEnd.on('mouseout', function (e) {
        end.color = "#000000";
        stage.update();
    });
    var recordsCharback = new createjs.Shape();
    recordsCharback.graphics.beginFill("#EB7F00").drawRect(0,0,800,50);
    recordsCharback.y = btnBackgroundEnd.y + 160;
    endBtn.addChild(recordsCharback);
    stage.update();
    var recordsText = new createjs.Text('Click Here to Browse Your Records', '36px Irish Grover', "#000000");
    recordsText.className = "hoverable";
    recordsText.x = exDescriptionEnd.x;//(stage.getBounds().width/2) - recordsText.getBounds().width/2;
    recordsText.y = recordsCharback.y;
    recordsText.regX = recordsText.getBounds().width/2;
    endBtn.addChild(recordsText);
    stage.update();

    recordsCharback.on('click', function () {
        window.location = '/dashboard';
    });
    recordsCharback.on('mouseover', function (e) {
        recordsText.color = "#0000ff";
        stage.update();
    });
    recordsCharback.on('mouseout', function (e) {
        recordsText.color = "#000000";
        stage.update();
    });
    //console.log('MissedKeys::: '+JSON.stringify(missedKeys.keylist));
    //data = JSON.stringify(data);
    //missedKeys.k
    //console.log(data);
    var exData =
    {
        exercise: (+(6 * (exId)) + (whichercise.valueOf() - 1)).toString(),
        completionTime: gameTimer,
        rate: rate,
        missedKeys: missedKeyss
    };
    exData = JSON.stringify(exData);
    console.log((new Date().getTime())+'\n'+exData);
    jqAjax.ajax({
        url: 'submit',
        type: 'post',
        data:{
            exData: exData
        },
        success: function(data){
            hideSelect(running);
            missedKeys = [];
            console.log('saved exercise data: '+(new Date().getTime())+'\n'+exData);
        },
        error: function(xhr,status){
            alert('Error submitting exercise data: '+status);
        },
        complete: function(xhr,status){
            //alert('complete:'+status);
        }
    });

    //post('/exercises/submit', );

}
function addFilterTo(shape, correct, i, foxLength)
{
    var color = 0xFFFF00;
    if (!correct)
    {
        color = 0xCC000E;
        createjs.Sound.play("error.wav");
    }
    else
        createjs.Sound.play("success.wav");
    var alpha = 2;
    var blurX = 10;
    var blurY = 10;
    var strength = 5;
    var quality = 100;
    var inner = true;
    var knockout = false;
    var _glowFilter = new createjs.GlowFilter(color, alpha, blurX, blurY, strength, quality, inner, knockout);
    shape.filters = [_glowFilter];
    shape.color = "#000000";
    shape.cache(0,0,100,100);
    console.log('adding '+correct+' filter to letter '+shape.text+' at index: '+i+'/'+fox.length);
    if (i == (foxLength))
    {
        var rate = Math.floor(( (fox.length/canter) / (gameTimer / 60)) * 100) / 100;
        running = false;
        showEndGame(rate, 3, missedKeys);

    }
}
function buildCanvas()
{
    if (hitbar)
        stage.removeChild(hitbar);
    hitbar = new createjs.Shape();
    hitbar.graphics.beginFill("#aa0000").drawRect(0, 0, 40, canvas.height);
    hitbar.x = 100;
    hitbar.y = 0;
    //hitbar.width = 50;
    hitbar.graphics.opacity = 0.5;
    hitbar.regX = hitbar.width/2;
    hitbar.regY = hitbar.height/2;


    if (timerDisplay)
        stage.removeChild(timerDisplay);
    timerDisplay = new createjs.Text("Time Elapsed: "+0, "24px Monaco", "#000");
    timerDisplay.x = 25;
    timerDisplay.y = 25;
    //timerDisplay.regX = timerDisplay.getBounds().width/2;
    timerDisplay.regY = timerDisplay.getBounds().height/2;

    if (scoreDisplay)
        stage.removeChild(scoreDisplay);
    scoreDisplay = new createjs.Container();
    var scoreBg = new createjs.Shape();
    scoreBg.graphics.beginFill("#afa0aa").drawRect(0,0,canvas.width, 40);
    scoreBg.x = 0;
    scoreBg.y = 0;
    //var scoreText = new createjs.Text("Score: "+hiddenscr, "24px Monaco", "#000");
    //scoreText.x = 650;
    //scoreText.y = timerDisplay.y;
    //scoreText.regX = scoreText.getBounds().width/2;
    //scoreText.regY = scoreText.getBounds().height/2;

    scoreDisplay.addChild(scoreBg, timerDisplay);
    stage.addChild(hitbar);
    stage.addChild(scoreDisplay);
    stage.enableMouseOver(30);
}



function processInput(event)
{
    if (event.keyCode == 13)
    {
        if (!running)
            startGame();
        console.log('pressing ENTER');
        event.stopPropagation();
    }
    var keypressed = handleInput(event);
    pressedKeys = [];
    pressedKeys[0] = keypressed;
    console.log('keypressed: '+keypressed);
    return event;
}

function dokeydown(event)
{
    //var tevent = event;
    console.log('keydown: '+(event.which));
    if ((event.keycode || event.charCode || event.which) === 32)
    {
        event.preventDefault();
        velocity = 15;

    }

    return event;

}

function dokeyup(event)
{
    if ((event.keycode || event.charCode || event.which) === 32)
        velocity = baseVelocity;
}

function updateThisGame()
{
    //pressedKeys = [];
    //for (var i = 0; i < letterContainer.numChildren; i++)
    //{
    //    var tletter = letterContainer.getChildAt(i);
    //    tletter.update();
    //    //tletter.x-=velocity;
    //    //if ((tletter.x < (+hitbar.x+30)) && (tletter.x > (+hitbar.x-30)) && (pressedKeys[0] === tletter.text))
    //    //{
    //    //    pressedKeys.shift();
    //    //    console.log('checking collision');
    //    //    hiddenscr += 5;
    //    //    scoreDisplay.getChildAt(2).text = "Score: "+hiddenscr;
    //    //}
    //    //hitOnce = false;
    //}
    pressedKeys.shift();
}

function main()
{
    setupCanvas('#ffffff'); //sets up the canvas
    var background = {};
    stage.enableMouseOver();

    //document.body.addEventListener("keydown", dokeydown, false);
    //document.body.addEventListener("keyup", dokeyup, false);
    //document.body.addEventListener("keypress", processInput, false);
    setText({value:tpid});


    document.addEventListener("updategame", updateThisGame);

    var splat = new createjs.Bitmap(loader.getResult("steel.jpg"));
    splat.x = 0;
    splat.y = 0;
    splat.width = 800;
    splat.height = 600;
    splat.scaleX = 1;
    splat.scaleY = 1;
    //splat.regX = splat.getBounds().width/2;
    //splat.regY = splat.getBounds().height/2;
    //stage.removeChild(circle, myText);
    splat.filters = [new createjs.ColorFilter(0.7,0.7,0.7,0.9)];
    stage.addChild(splat);
    splat.cache(0,0, 800, 600);
    stage.update();


    //createjs.EventDispatcher.initialize(Card.prototype);

    //document.addEventListener('keypress', handleInput);
    buildCanvas();
    //hideSelect(false);
    var el = document.getElementById('trainerSelector');
    el.selectedIndex = tpid-1;
    el.disabled = running;
    document.getElementById('speedSelector').disabled = false;
    showStartBtn();
    canvas.onclick = startGame;

    document.addEventListener("keypress", processInput, false);
    onResize();
    //el.scrollIntoView(true);
    canvas.focus();
    canvas.tabIndex = 1;
    //el.focus();
}
if (!!window.addEventListener)
{

    window.addEventListener("DOMContentLoaded", init(["../images/steel.jpg","../sounds/success.wav","../sounds/error.wav"]), false);
}
else
{
    window.attachEvent('onload', init(["../images/steel.jpg","../sounds/success.wav","../sounds/error.wav"]));
}
