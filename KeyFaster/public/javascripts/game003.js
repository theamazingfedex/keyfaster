    var FPS = 30;

    var stage = null;
    var canvas, basket;
    var whichercise = "1";
    var gameTimer = 0;
    var hiddenscr = 0;
    var frameCount = 0;
    var deadline = 580;
    var basketline = 10;
    var timerDisplay, myTween, startBtn, running, basketContainer, btext, endBtn;
    var letterCollection, lastKeyPressed, brokenApple, apple;
    var pressedKeys = [];
    var keyerrors = [];
    var hitRightKey = false;
    var keyexpected = [];
    var pressed = false;
    var position = 0;
    var fox = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];
    var text7 = ['w', 'W', 'a', 'A', 's', 'S', 'd', 'D', 'w', 'a', 's', 'd', 'W', 'A', 'S', 'D', 'W', 's', 'W', 'A', 'D', 'S', 'w', 'w', 'W', 'd'];
    var text8 = ['q', 'w', 'e', 'r', 't', 'd', 'f', '1', '2', '3', '4', '5', 'w', 'r', 'e', 'q'];
    var text9 = ['t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x', 'j', 'u', 'm', 'p', 's', 'o', 'v', 'e', 'r', 'a', 'l', 'a', 'z', 'y', 'd', 'o', 'g'];

    function startGame()
    {
        if (!running) {

            running = true;
            hideSelect(running);
            if (startBtn)
                stage.removeChild(startBtn);
            if (endBtn)
                stage.removeChild(endBtn);

            //if (timerDisplay)
            //    stage.removeChild(timerDisplay);
            stage.addChild(letterCollection);
            position = 0;
            addBasket();
            addNextLetter();
        }
    }

    function setupCanvas() {
        canvas = document.getElementById("canvas");

        //ctx = canvas.getContext('2d');
        canvas.width = 800;
        canvas.height = 600;
        stage = new createjs.Stage(canvas);
        letterCollection = new createjs.Container();
        //startContainer = new createjs.Container();

        var background = new createjs.Shape();
        //background.graphics.beginFill('#4cbe4c').drawRect(0, 0, canvas.width, canvas.height);
        background.graphics.beginLinearGradientFill(["#00A300","#4cbe4c"], [0, 1], canvas.height/2,0,  canvas.width/2, canvas.height).drawRect(0, 0, canvas.width, canvas.height);
        background.x = 0;
        background.y = 0;
        stage.addChild(background);

        var bgImg = new createjs.Bitmap(loader.getResult("appletree.png"));
        bgImg.scaleX *= 1.3;
        bgImg.scaleY *= 1;
        bgImg.regX = bgImg.getBounds().width/2;
        bgImg.regY = bgImg.getBounds().height/3;
        bgImg.x = canvas.width/2;
        stage.addChild(bgImg);
        timerDisplay = new createjs.Text("Time Elapsed: " + gameTimer, "14px Arial", "#000000");
        timerDisplay.x = canvas.width / 2;
        timerDisplay.y = canvas.height - (timerDisplay.getBounds().height);
        timerDisplay.regX = timerDisplay.getBounds().width / 2;
        timerDisplay.regY = timerDisplay.getBounds().height / 2;

        //var my_gradient= new createjs.Graphics();//0,0,0,(canvas.height/3 )* 2);
        //my_gradient.beginLinearGradientFill(["#777","#BBB"], [0, 1], 0, (canvas.height/3 )* 2, canvas.width, (canvas.height/3 )* 2).drawRect(0, 0, canvas.width, canvas.height);
        //
        //stage.addChild(my_gradient);
        stage.addChild(timerDisplay);

        showStartBtn();


        //tweenObj(timerDisplay, 140 ,280);

        //letter1 =
        canvas.onclick = startGame;
    }

    function showStartBtn()
    {
        var r = 255;//genCol();
        var g = 255;//genCol();
        var b = 255;//genCol();
        stage.enableMouseOver();

        startBtn = new createjs.Container();

        var btnBackground = new createjs.Shape();
        btnBackground.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
        btnBackground.width = 800;
        btnBackground.height = 300;
        btnBackground.regX = 400;
        btnBackground.regY = 150;
        btnBackground.x = canvas.width/2;
        btnBackground.y = canvas.height/2;
        startBtn.addChild(btnBackground);
        stage.update();
        var exName = new createjs.Text('Typing for Apples', '36px Irish Grover', isWhiteText(r,g,b,1));
        exName.x = canvas.width/2;
        exName.y = canvas.height/2 - 100;
        exName.regX = exName.getBounds().width/2;
        exName.regY = exName.getBounds().height/2;
        startBtn.addChild(exName);
        stage.update();
        var exDescription = new createjs.Text(
            'Tips for playing:\n'+
            '1. Place your fingers on the home row of your keyboard.\n' +
            '2. Press the displayed key as soon as it shows up.\n' +
            '3. Catch apples in the basket to score.\n' +
            '4. Get a perfect score on the same program 5 times in a row to\n' +
            '   reset your error history for that specific training program.',
            "20px Architects Daughter", isWhiteText(r,g,b,1));
        exDescription.x = exName.x - exDescription.getBounds().width/2;
        exDescription.y = exName.y + 20;
        startBtn.addChild(exDescription);
        stage.update();

        var exCharback = new createjs.Shape();
        exCharback.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
        exCharback.y = btnBackground.y + 100;
        startBtn.addChild(exCharback);
        stage.update();
        var startGame = new createjs.Text('Click or press ENTER to Begin', '36px Irish Grover', "#000000");
        startGame.className = "hoverable";
        startGame.x = exName.x - startGame.getBounds().width/2;
        startGame.y = exCharback.y;
        startBtn.addChild(startGame);
        stage.update();

        exCharback.on('mouseover', function (e) {
            startGame.color = "#0000ff";
            stage.update();
        });
        exCharback.on('mouseout', function (e) {
            startGame.color = "#000000";
            stage.update();
        });

        //stage.addChild(startBtn);
        //startTxt = new createjs.Text('Click to Begin', "24px Arial", isWhiteText(r,g,b,0.9));
        //stage.addChild(startTxt);
        stage.addChild(startBtn);

    }
    function updateGame() {
        for (var i = 0; i < letterCollection.numChildren; i++) {
            var letterchild = letterCollection.getChildAt(i);
            letterchild.update();
            tweenLetter(letterchild, letterchild.x, deadline + 50, letterchild.velocity);
            if (hiddenscr < 0)
                hiddenscr = 0;
            btext.text = 'Pressed: ' + (lastKeyPressed || ' ') + '';// + ' Score:' + hiddenscr;
        }
        //letterCollection.numChildren
    }

    function addBasket() {

        basketContainer = new createjs.Container();
        basket.x = 10;
        basket.y = 500;
        basket.regX = basket.getBounds().width / 2;
        basket.regY = basket.getBounds().height / 2;
        basket.scaleX = 200 / basket.getBounds().width;
        basket.scaleY = 200 / basket.getBounds().height;

        var bcircle = new createjs.Shape();
        btext = new createjs.Text('Pressed: ' + (lastKeyPressed || ' ') + '', "24px Irish Grover", '#000000');
        btext.x = basket.x - 75;
        btext.y = basket.y + 12;
        btext.regX = btext.width / 2;
        btext.regY = btext.height / 2;

        bcircle.graphics.beginFill(createjs.Graphics.getRGB(255, 255, 255, 0.9)).drawRect(btext.x, btext.y, 135, 24);
        bcircle.regX = bcircle.width / 2;
        bcircle.regY = bcircle.height / 2;
        basketContainer.addChild(basket, bcircle, btext);
        basketContainer.x += 200;
        //basketContainer.regX = basketContainer.getBounds().width/2;
        //basketContainer.regY = basketContainer.getBounds().height/2;
        stage.addChild(basketContainer);
    }

    function addNextLetter() {
        hitRightKey = false;
        if (position >= fox.length) {
            running = false;
            var kerrors = [];
            for (var i = 0; i < keyerrors.length; i++) {
                kerrors[kerrors.length] = {missed: keyexpected[i], typed: keyerrors[i]};
            }
            var canter = 1;
            var rate = Math.floor(( (fox.length/canter) / (gameTimer / 60)) * 100) / 100;
            //alert('Score:: ' + hiddenscr + '\nmissedKeys:: ' + JSON.stringify(kerrors));
            console.log('ended game exData:');
            //if (whichercise === "1")
            showEndGame(rate, 2, kerrors);
                //post('/exercises/submit', {
                //    exercise: (+(6*2)+(whichercise.valueOf()-1)).toString(),
                //    completionTime: gameTimer,
                //    rate: rate,
                //    missedKeys: JSON.stringify(kerrors)
                //});
            //if (whichercise === "2")
            //    post('/exercises/two', {
            //        exercise: whichercise,
            //        completionTime: gameTimer,
            //        rate: rate,
            //        missedKeys: JSON.stringify(kerrors)
            //    });
            //if (whichercise === "3")
            //    post('/exercises/three', {
            //        exercise: whichercise,
            //        completionTime: gameTimer,
            //        rate: rate,
            //        missedKeys: JSON.stringify(kerrors)
            //    });
        }
        else {
            var velocity = (Math.floor(Math.random() * (fox.length / 2)));
            if (velocity < 5)
                velocity += 5;
            var delay = 0;
            //console.log('velocity of letter ' + position + ' :: ' + velocity);
            addLetterToCanvas(20 + (Math.random() * (800 - 20)), -30, fox[position], velocity, delay);
            position++;
        }
    }

    (function (window) {
        function Letter(velocity, delay) {

            this.initialize();
            this.velocity = velocity;
            this.delay = delay;
        }

        //var tickCounter = 0;
        //Inheritance from Container
        Letter.prototype = new createjs.Container();
        Letter.prototype.Container_initialize = Letter.prototype.initialize;
        Letter.prototype.Container_tick = Letter.prototype._tick;

        Letter.prototype.initialize = function () {
            //call to initialize() method from parent class
            this.Container_initialize();

            //console.log("Letter initialized");
        };
        Letter.prototype._tick = function () {
            //call to _tick method from parent class
            this.Container_tick();

            //console.log("Letter Ticked");
        };

        Letter.prototype.onKeypress = function (event) {
            //handleInput(event);
            if (event.keyCode == 13)
            {
                startGame();
            }
            else
            {
                var tletter = letterCollection.getChildAt(0);
                pressedKeys[0] = handleInput(event);
                if (tletter.getChildAt(2).text === pressedKeys[0]) {
                    //hiddenscr += 5;
                    console.log(JSON.stringify(tletter.getBounds().x));
                    updateKeyline(tletter.getBounds().x);
                    if (hitRightKey)
                        tletter.velocity += 10;
                    hitRightKey = true;
                    //pressed = false;

                }
                else if (tletter.getChildAt(2).text != pressedKeys[0]) {
                    hitRightKey = false;
                    if (!pressed) {
                        hiddenscr -= 3;
                        pressed = true;
                        keyerrors.push(pressedKeys.shift());
                        keyexpected.push(tletter.getChildAt(2).text);
                        //letterCollection.removeChild(tletter);
                        //addNextLetter();
                    }
                }
            }
            preventDefault(event);
        };
        Letter.prototype.update = function () {
            //winning some points
            if (this.y > deadline && basketContainer.x <= (+this.getBounds().x + 100) && basketContainer.x >= (+this.getBounds().x - 100)) {
                if (hitRightKey) {
                    hiddenscr += 5;
                    createjs.Sound.play("success.wav");
                }
                else //caught the apple, but with the wrong key
                {
                    hiddenscr -= 3;
                    keyerrors.push(pressedKeys.shift() || 'missed');
                    keyexpected.push(this.getChildAt(2).text);
                    addBrokenLetter(this.getBounds().x, this.y - 80);
                }
                //console.log('HITbasketX:: '+basketContainer.x + '=========  letterXY:: '+this.getBounds().x + ', '+this.y);
                //console.log('caught an apple, currentscore:: ' + hiddenscr);
                pressed = false;
                letterCollection.removeChild(this);
                addNextLetter();
            }
            else if (this.y > deadline + 40)//didn't catch it in time, you get NOTHING!! MWAHAHAHA
            {
                //console.log('MISSED basketX:: '+basketContainer.x + '=========  letterXY:: '+this.getBounds().x + ', '+this.y);
                if (hitRightKey)
                    hiddenscr -= 3;
                else
                    hiddenscr -= 4;
                keyerrors.push(pressedKeys.shift() || 'missed');
                keyexpected.push(this.getChildAt(2).text);
                pressed = false;
                letterCollection.removeChild(this);
                addBrokenLetter(this.getBounds().x, this.y - 80);
                addNextLetter();
            }
        };

        window.Letter = Letter;
    }(window));

    function showEndGame(rate, exId, missedKeys)
    {
        tumbling = false;
        running = false;
        canvas.onclick = function ()
        {
            //boop
        };
        endBtn = new createjs.Container();

        var btnBackgroundEnd = new createjs.Shape();
        btnBackgroundEnd.graphics.beginFill("#F9C647").drawRect(0, 0, 800, 300);
        btnBackgroundEnd.y = 300;//stage.getBounds().height/2;
        btnBackgroundEnd.x = 400;//stage.getBounds().width/2;
        btnBackgroundEnd.width = 800;//stage.getBounds().width;
        btnBackgroundEnd.height = 300;//stage.getBounds().height/2;
        btnBackgroundEnd.regX = btnBackgroundEnd.width/2;
        btnBackgroundEnd.regY = btnBackgroundEnd.height/2;
        endBtn.addChild(btnBackgroundEnd);
        stage.update();
        var exNameEnd = new createjs.Text('Typing for Apples Results', '36px Irish Grover', '#000');
        exNameEnd.x = btnBackgroundEnd.width/2;
        exNameEnd.y = btnBackgroundEnd.height - 100;
        exNameEnd.regX = exNameEnd.getBounds().width/2;
        exNameEnd.regY = exNameEnd.getBounds().height/2;
        endBtn.addChild(exNameEnd);
        stage.update();
        var missedKeyList = missedKeys;
        var missedKeyString = '';
        for ( var i = 0; i < missedKeyList.length; i++)
        {
            if (typeof missedKeyList[i].missed != 'undefined' || missedKeyList[i].missed != '') {
                missedKeyString += missedKeyList[i].missed;
                if (i != missedKeyList.length-1)
                    missedKeyString+=', ';
                else {
                    if (missedKeyString === '')
                        missedKeyString += 'None; Perfect Score!';
                }
                if ((i+1) % 6 == 0)
                    missedKeyString += '\n               ';
            }
        }
        //missedKeyString = missedKeyString.split(',');
        //missedKeyString[Math.floor(missedKeys.keylist.length/2)] += ' ';
        //if (missedKeys.keylist.length > 4)

        var exDescriptionEnd = new createjs.Text(
            '\nWPM: '+rate + '\n'+
            'Completion Time: '+gameTimer+'\n'+
            'Training Program: '+getSelectionText()+'\n'
            ,
            "20px Architects Daughter", '#000');

        exDescriptionEnd.lineWidth = 600;
        //exDescriptionEnd.maxWidth = 600;
        exDescriptionEnd.x = exNameEnd.x;
        exDescriptionEnd.y = exNameEnd.y + 20;
        exDescriptionEnd.regX = exDescriptionEnd.getBounds().width/2;
        endBtn.addChild(exDescriptionEnd);
        stage.update();
        var missedKeyResults = new createjs.Text('Missed Keys: '+ missedKeyString, "24px Architects Daughter", '#ff0000');
        missedKeyResults.lineWidth = 600;
        //missedKeyResults.maxWidth = 600;
        missedKeyResults.x = exNameEnd.x - 20;
        missedKeyResults.y = exNameEnd.y + 100;
        missedKeyResults.regX = exDescriptionEnd.getBounds().width/2;
        endBtn.addChild(missedKeyResults);
        stage.update();

        var exCharbackEnd = new createjs.Shape();
        exCharbackEnd.graphics.beginFill("#FFFF66").drawRect(0,0,800,50);
        exCharbackEnd.y = btnBackgroundEnd.y + 110;
        endBtn.addChild(exCharbackEnd);
        stage.update();
        var end = new createjs.Text('Press ENTER to try again', '36px Irish Grover', "#000000");
        end.className = "hoverable";
        end.x = exDescriptionEnd.x;
        end.y = exCharbackEnd.y;
        end.regX = end.getBounds().width/2;
        //end.regY = end.getBounds().height/2;
        endBtn.addChild(end);
        stage.addChild(endBtn);
        onResize();
        stage.update();

        exCharbackEnd.on('mouseover', function (e) {
            end.color = "#0000ff";
            stage.update();
        });
        exCharbackEnd.on('mouseout', function (e) {
            end.color = "#000000";
            stage.update();
        });
        var recordsCharback = new createjs.Shape();
        recordsCharback.graphics.beginFill("#EB7F00").drawRect(0,0,800,50);
        recordsCharback.y = btnBackgroundEnd.y + 160;
        endBtn.addChild(recordsCharback);
        stage.update();
        var recordsText = new createjs.Text('Click Here to Browse Your Records', '36px Irish Grover', "#000000");
        recordsText.className = "hoverable";
        recordsText.x = exDescriptionEnd.x;//(stage.getBounds().width/2) - recordsText.getBounds().width/2;
        recordsText.y = recordsCharback.y;
        recordsText.regX = recordsText.getBounds().width/2;
        endBtn.addChild(recordsText);
        stage.update();

        recordsCharback.on('click', function () {
            window.location = '/dashboard';
        });
        recordsCharback.on('mouseover', function (e) {
            recordsText.color = "#0000ff";
            stage.update();
        });
        recordsCharback.on('mouseout', function (e) {
            recordsText.color = "#000000";
            stage.update();
        });
        //console.log('MissedKeys::: '+JSON.stringify(missedKeys.keylist));
        //data = JSON.stringify(data);
        //missedKeys.k
        //console.log(data);
        var exData =
        {
            exercise: (+(6 * (exId)) + (whichercise.valueOf() - 1)).toString(),
            completionTime: gameTimer,
            rate: rate,
            missedKeys: missedKeys
        };
        exData = JSON.stringify(exData);
        console.log((new Date().getTime())+'\n'+exData);
        jqAjax.ajax({
            url: 'submit',
            type: 'post',
            data:{
                exData: exData
            },
            success: function(data){
                hideSelect(running);
                console.log('saved exercise data: '+(new Date().getTime())+'\n'+exData);
            },
            error: function(xhr,status){
                    alert('Error submitting exercise data: '+status);
            },
            complete: function(xhr,status){
                //alert('complete:'+status);
            }
        });

        //post('/exercises/submit', );

    }
    function addBrokenLetter(x, y) {
        brokenApple = new createjs.Bitmap("../images/brokenapple.png");
        brokenApple.x = x;
        brokenApple.y = y;
        brokenApple.scaleX = 80 / brokenApple.getBounds().width;
        brokenApple.scaleY = 80 / brokenApple.getBounds().height;
        brokenApple.regX = brokenApple.getBounds().width / 2;
        brokenApple.regY = brokenApple.getBounds().height / 2;
        createjs.Sound.play("splat.wav");
        stage.addChild(brokenApple);
        tweenFade(brokenApple, 1000);
    }

    function updateKeyline(left) {
        tweenObj(basketContainer, left, basketContainer.y);
    }

    function genCol() {
        return Math.floor(Math.random() * 255);
    }

    function isWhiteText(R, G, B, gamma) {
        R = R / 255;
        G = G / 255;
        B = B / 255;
        gamma = 2.2;
        var L = 0.2126 * Math.pow(R, gamma)
            + 0.7152 * Math.pow(G, gamma)
            + 0.0722 * Math.pow(B, gamma);

        var using_black = ( L > 0.4 );
        if (using_black) {
            return '#000000';
        }
        else if (!using_black) {
            return '#ffffff';
        }
    }

    function addLetterToCanvas(left, top, ch, velocity, delay) {
        if (left < 40)
            left += 40;
        if (left > 760)
            left -= 40;

        basketline = left;

        var circle = new createjs.Shape();
        var r = 255;//genCol();
        var g = 255;//genCol();
        var b = 255;//genCol();
        //console.log('color: r('+r+')  g('+g+')  b('+b+')');
        circle.graphics.beginFill(createjs.Graphics.getRGB(r, g, b, 0.9)).drawCircle(left, top, 20);
        apple.x = left;
        apple.y = top;
        apple.scaleX = 80 / apple.getBounds().width;
        apple.scaleY = 80 / apple.getBounds().height;
        apple.regX = apple.getBounds().width / 2;
        apple.regY = apple.getBounds().height / 2;
        var myText = new createjs.Text(ch, "24px Arial", isWhiteText(r, g, b, 0.9));
        myText.regX = myText.getBounds().width / 2;
        myText.regY = myText.getBounds().height / 2;
        myText.x = left;
        myText.y = top;
        var myLetter = new Letter(velocity, delay);
        myLetter.addChild(apple, circle, myText);
        myLetter.regX = myLetter.getBounds().width / 2;
        myLetter.regY = myLetter.getBounds().height / 2;
        //myLetter.addEventListener('keypress', function (event) {
        //
        //});
        letterCollection.addChild(myLetter);
    }

    function main() {
        //basket = new createjs.Bitmap("../images/basket.png");
        //apple = new createjs.Bitmap("../images/apple.png");
        //brokenApple = new createjs.Bitmap("../images/brokenapple.png");
        setText({value:tpid});
        setupCanvas(); //sets up the canvas
        createjs.EventDispatcher.initialize(Letter.prototype);
        console.log('initialized LetterDispatcher');
        createjs.Ticker.on("tick", loop);
        createjs.Ticker.setFPS(FPS);
        document.addEventListener('keypress', Letter.prototype.onKeypress);
        onResize();
        var el = document.getElementById('trainerSelector');
        //el.value = "1";
        el.selectedIndex = tpid-1;;
        //el.scrollIntoView(true);
        el.focus();

        //document.addEventListener('keypress', function (event) {
        //    handleInput(event);
        //    preventDefault(event);
        //    //letterCollection.fireEvent('keypress', event);
        //});
    }

    function checkCollision(obj1, obj2) {
        if (obj1.hitTest(obj2.x, obj2.y))
            return true;
    }

    function handleInput(event) {
        var keypressed = '';
        //save the input pressed for the update function to capture and act upon
        keypressed = getChar(event);
        if (keypressed === " ")
            keypressed = "space";
        pressedKeys.push(keypressed);
        //if (keypressed)
            lastKeyPressed = keypressed || ' ';
        //console.log('pressed:: '+keypressed);
        //timerDisplay.x -= 5;
        //timerDisplay.y -= 5;
        return keypressed;
    }

    function getChar(event) {

        if (event.which == null) {
            return String.fromCharCode(event.keyCode); // IE
        } else if (event.which != 0 && event.charCode != 0) {
            //console.log(String.fromCharCode(event.which));
            return String.fromCharCode(event.which);  // the rest
        } else {
            return null; // special key
        }
    }

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function resetGameTimer() {
        gameTimer = 0;
    }

    function runGameTimer() {
        if (running) {
            frameCount += 1;
            if (frameCount % (FPS / 10) === 0) {
                gameTimer = frameCount / (FPS);
                timerDisplay.text = "Time Elapsed: " + gameTimer;
            }
        }
    }

    function post(path, params, method) {
        method = method || "post";
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }

    function tweenComplete(tween) {
        //tween.remove();
        //console.log("Tween Complete!");
    }

    function tweenObj(myObject, desX, desY) {
        myTween = createjs.Tween.get(myObject, {loop: false, override: true})
            .wait(0)
            .to({x: desX, y: desY, rotation: 0}, 500)
            .call(tweenComplete);

    }

    function tweenLetter(myObject, desX, desY, velocity) {
        var letterTween = createjs.Tween.get(myObject, {loop: false})
            .wait(0)
            .to({x: desX, y: desY, rotation: 0}, (+velocity * 230))
            .call(tweenComplete);
    }

    function tweenFade(myObject, ttl) {
        var fadeTween = createjs.Tween.get(myObject, {loop: false, override: true})
            .wait(+ttl / 2)
            .to({alpha: 0}, (+ttl / 2))
            .call(tweenComplete);
    }


    function loop(event) {
        if (!event.paused) {
            if (running) {
                runGameTimer();
                updateGame();
            }
            //letter1.position.x = 0;
            stage.update();
        }
    }


//makes sure DOM is ready then loads main()
    if (!!(window.addEventListener)) {
        basket = new createjs.Bitmap("../images/basket.png");
        apple = new createjs.Bitmap("../images/apple.png");
        brokenApple = new createjs.Bitmap("../images/brokenapple.png");
        window.addEventListener("DOMContentLoaded", init(["../images/appletree.png","../sounds/success.wav","../sounds/splat.wav"]), false);
    } else { //MSIE
        window.attachEvent("onload", init(["../images/appletree.png","../sounds/success.wav","../sounds/splat.wav"]));
    }