

var orw = 800, orh = 600;
this.window.onresize = function(){
  onResize(orw, orh);

};
onResize = function(ow, oh)
{

    // window dimensions
    var w = (+window.innerWidth*0.9);
    var h = (+window.innerHeight*0.9) - 150;

// stage dimensions
//    if ((ow && oh) && (!isNaN(ow) && !isNaN(oh)))
//    {
//        ow = ow || 800;
//        oh = oh || 600;
//        orw = ow;
//        orh = oh;
//
//    }
//    else
//    {
//        ow = 800;
//        oh = 600;
//    }
    //var ow = document.getElementById('canvas').clientWidth;
    //var oh = document.getElementById('canvas').clientHeight;
    var keepAspectRatio = ((w/h)===(ow/oh)) ? true : false;
    //resize the stage.children
    //var stage = stage;
    if (typeof stage != 'undefined') {
        ow = 800;
        oh = 600;
        keepAspectRatio = ((w/h)===(ow/oh)) ? true : false;
        if (!keepAspectRatio) {
            // keep aspect ratio
            var scale = Math.min(w / ow, h / oh);
            stage.scaleX = scale;
            stage.scaleY = scale;
            for (var i = 0; i < stage.numChildren; i++) {
                stage.getChildAt(i).width = stage.getChildAt(i).width * scale;
                stage.getChildAt(i).height = stage.getChildAt(i).height * scale;

            }

            // adjust canvas size
            stage.canvas.width = (ow * scale);
            stage.canvas.height = (oh * scale);
        }
        else {
            // scale to exact fit
            stage.scaleX = w / ow;
            stage.scaleY = h / oh;

            // adjust canvas size
            stage.canvas.width = ow * stage.scaleX;
            stage.canvas.height = oh * stage.scaleY;
        }

        // update the stage
        stage.update()
    }
    else if (typeof myGraph != 'undefined')
    {
        var rcanvas = document.getElementById('rategraph');
        var tcanvas = document.getElementById('timegraph');
        var rctx = rcanvas.getContext('2d');
        var tctx = tcanvas.getContext('2d');

        ow = 400;
        oh = 300;
        keepAspectRatio = ((w/h)===(ow/oh)) ? true : false;
        var scale = Math.min(w / ow, h / oh);
        if (!keepAspectRatio) {
            // keep aspect ratio
            //rctx.scaleX = scale;
            //rctx.scaleY = scale;
            //tctx.scaleX = scale;
            //tctx.scaleY = scale;
            ////rctx.scale(scale, scale);
            ////tctx.scale(scale, scale);
            ////for (var i = 0; i < stage.numChildren; i++) {
            ////    stage.getChildAt(i).width = stage.getChildAt(i).width * scale;
            ////    stage.getChildAt(i).height = stage.getChildAt(i).height * scale;
            ////
            ////}
            //
            //// adjust canvas size
            //myGraph.scaleX = scale;
            //myGraph.scaleY = scale;
            //myTGraph.scaleX = scale;
            //myTGraph.scaleY = scale;
            //rctx.canvas.width =  (ow * scale);
            //rctx.canvas.height = (oh * scale);
            //tctx.canvas.width =  (ow * scale);
            //tctx.canvas.height = (oh * scale);
            //rcanvas.width =  (ow * scale);
            //rcanvas.height = (oh * scale);
            //tcanvas.width =  (ow * scale);
            //tcanvas.height = (oh * scale);

            //myGraph.resize();
            //myTGraph.resize();
        }
        else {
            // scale to exact fit
            //rctx.scaleX = w / ow;
            //rctx.scaleY = h / oh;
            //tctx.scaleX = w / ow;
            //tctx.scaleY = h / oh;
            //
            ////rctx.scale(w/ow, h/oh);
            ////tctx.scale(w/ow, h/oh);
            //// adjust canvas size
            ////myGraph.scaleX = scale;
            ////myGraph.scaleY = scale;
            ////myTGraph.scaleX = scale;
            ////myTGraph.scaleY = scale;
            //rcanvas.width =  ow * rctx.scaleX;
            //rcanvas.height = oh * rctx.scaleY;
            //tcanvas.width =  ow * tctx.scaleX;
            //tcanvas.height = oh * tctx.scaleY;
            //
            //rctx.canvas.width =  ow * rctx.scaleX;
            //rctx.canvas.height = oh * rctx.scaleY;
            //tctx.canvas.width =  ow * tctx.scaleX;
            //tctx.canvas.height = oh * tctx.scaleY;
            rcanvas.resize();
            tcanvas.resize();
            //myGraph.resize();
            //myTGraph.resize();
        }
    }
}