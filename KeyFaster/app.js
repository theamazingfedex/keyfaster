var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var fbAppId = '822222177865649';
var fbAppSecret = '08ee1a33792e3807e6ec0a1bea301b41';
var twAppId = '2nI4o6D3FV0fKcdbzniW4CIQy';
var twAppSecret = 'dStKR1oNnPYTXRsVm54G6slhaTTyFKpXsAeAdOuGEVYg3y2E8K';
var gAppId = '643573980824-aff09m4tj9ebk0ptepqg1lp42tervjkr.apps.googleusercontent.com';
var gAppSecret = 'gCc8YcVZwsvyWeZgMOM4fQYP';
var winAppId = '000000004414EC66';
var winAppSecret = 'XJ8Q7qLfq8ElYXW7ojCfOTdHiDiJlbI0';

var app = express();
var dburl = 'mongodb://localhost/test';
var bCrypt = require('bcrypt-nodejs');
//var https = require('https');
var http = require('http');
var exitPort = 3000;
app.set('port', process.env.remotePort || exitPort);

//setup passport
var flash = require('connect-flash');
app.use(express.static(path.join(__dirname, '/public')));
app.use(flash());
var passport = require('passport');
var LocalStrategy = require('passport-local');
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy =  require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-openidconnect').Strategy;
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);
app.use(expressSession({
     secret:'Super Duper Secret Key That They Will Never Guess',
     cookie: {maxAge: 2592000000, theme:'default' },
     saveUninitialized: true,
     resave: true,
     store: new MongoStore({
         url: dburl,
         collection: 'sessions'
     }),
    facebook:{
        clientID:'822222177865649',
        clientSecret: '08ee1a33792e3807e6ec0a1bea301b41',
        callbackURL: 'http://localhost:'+exitPort+'/oauth/facebook/callback'
    },
    twitter:{
        clientID:'2nI4o6D3FV0fKcdbzniW4CIQy',
        clientSecret: 'dStKR1oNnPYTXRsVm54G6slhaTTyFKpXsAeAdOuGEVYg3y2E8K',
        callbackURL: 'http://localhost:'+exitPort+'/oauth/twitter/callback'
    },
    google:{
        clientID:'2nI4o6D3FV0fKcdbzniW4CIQy',
        clientSecret: 'dStKR1oNnPYTXRsVm54G6slhaTTyFKpXsAeAdOuGEVYg3y2E8K',
        callbackURL: 'http://localhost:'+exitPort+'/oauth/twitter/callback'
    }
 }));
app.use(passport.initialize());
app.use(passport.session());



//setup routes
var routes = require('./routes/index');
//var users = require('./routes/users');
//var userRoutes = require('./routes/users');
//var exerciseRoutes = require('./routes/exercises');


//Setup the database
var mongoose = require('mongoose');
var mongooseOptions =
{
    connectionPool:5
};
mongoose.connect(dburl, mongooseOptions);

var User = require('./models/User.js');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('js', require('ejs').renderFile);

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(require('stylus').middleware(path.join(__dirname, './public/stylesheets')));


app.use('/', routes);
//app.use('/user', userRoutes);
//app.use('/exercises', exerciseRoutes);

//app.use(redirectUnmatched);
//
//function redirectUnmatched(req, res, next)
//{
//    res.redirect('/');
//}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
saveOAuthUserProfile = function(req, profile, done) {
    User.findOne({
            provider: profile.provider,
            providerId: profile.providerId
        },
        function(err, user) {
            if (err) {
                return done(err);
            }
            else {
                if (!user) {
                    var possibleEmail = profile.email;
                    User.findOne({email:possibleEmail}, function(err, tuser) {
                        //profile.username = profile.name.givenName;

                        user = new User(profile);

                        user.save(function(err) {
                            if (err) {
                                //var message = _this.getErrorMessage(err);
                                //req.flash('error', message);
                                return res.redirect('/');
                            }

                            return done(err, user);
                        });
                    });
                }
                else {
                    return done(err, user);
                }
            }
        }
    );
};
//passport.use('windowslive',new WindowsStrategy({
//        clientID: winAppId,
//        clientSecret: winAppSecret,
//
//        callbackURL: "http://127.0.0.1:"+exitPort+"/auth/windowslive/callback"
//    },
//    function(accessToken, refreshToken, profile, done) {
//        process.nextTick(function () {
//
//            var providerData = profile._json;
//            //providerData.token = token;
//
//            //providerData.tokenSecret = tokenSecret;
//            var providerUserProfile = {
//                username: profile.displayName,
//                email: profile.emails[0].value,
//                provider: 'windows',
//                providerId: profile.id,
//                providerData: providerData
//            };
//            saveOAuthUserProfile(null, providerUserProfile, done);
//        });
//    }
//));
passport.use('google', new GoogleStrategy({
        clientID: gAppId,
        clientSecret: gAppSecret,
        callbackURL: "http://127.0.0.1:"+exitPort+"/auth/google/callback"
    },
    function(iss, sub, profile, accessToken, refreshToken, done) {
        var providerData = profile._json;
        //providerData.token = token;

        //providerData.tokenSecret = tokenSecret;
        var providerUserProfile = {
            username: profile.displayName,
            email: profile.email,
            provider: 'google',
            providerId: profile.id,
            providerData: providerData
        };
        saveOAuthUserProfile(iss, providerUserProfile, done);
    }
));
passport.use('twitter', new TwitterStrategy({
        consumerKey: twAppId,
        consumerSecret: twAppSecret,
        callbackURL: 'http://localhost:'+exitPort+'/oauth/twitter/callback',
        sReqToCallback: true
    },
    function(req, token, tokenSecret, profile, done) {
        var providerData = profile._json;
        providerData.token = token;
        providerData.tokenSecret = tokenSecret;
        var providerUserProfile = {
            username: profile.displayName,
            //email: profile.emails[0].value,
            provider: 'twitter',
            providerId: profile.id,
            providerData: providerData
        };

        saveOAuthUserProfile(req, providerUserProfile, done);
    }));
passport.use('facebook',new FacebookStrategy({
        clientID: '822222177865649',
        clientSecret: '08ee1a33792e3807e6ec0a1bea301b41',
        callbackURL: 'http://localhost:'+exitPort+'/oauth/facebook/callback',
        passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
        var providerData = profile._json;
        providerData.accessToken = accessToken;
        providerData.refreshToken = refreshToken;

        var providerUserProfile =  new User();
        providerUserProfile.username = profile.name.givenName;
        providerUserProfile.email = profile.emails[0].value;
        //providerUserProfile.displayName = username: profile.username;
        providerUserProfile.provider = 'facebook';
        providerUserProfile.providerId = profile.id;
        providerUserProfile.providerData = providerData;

        saveOAuthUserProfile(req, providerUserProfile, done);
    }));

//var     getNewExerciseArray = function () {
//    var exerciseArray = [];
//    for (var p = 0; p < 19; p++)
//    {
//        //console.log(p);
//        exerciseArray[p] = {exercise:1+p, bestTime: 0, avgCompletionTime:0, bestRate: 0, avgRate:0, xCompleted:0, missedKeys:getNewKeyArray()};
//    }
//    //console.log('sexercise array position 1::: '+JSON.stringify(exerciseArray[2])+'   and position2:::' + JSON.stringify(exerciseArray[1]));
//    return exerciseArray;
//};
passport.use('signup', new LocalStrategy(
        {
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done)
        {
            username = convertToUppercase(username);
            var email = req.body.email;
            //console.log('---------------------------------------email: '+email+'\n--------------username: '+username);
            findOrCreateUser = function()
            {
                // find a user in Mongo with provided username
                User.findOne({ 'email' :  email }, function(err, user)
                {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('');
                        return done(null, false, req.flash('message','That E-mail is already in use'));
                    } else {
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.email = email;
                        newUser.theme = req.body.theme || 'default';
                        newUser.provider = 'local';
                        newUser.tpid = 1;
                        req.session.hasSetTP = false;
                        //newUser.exerciseData = getNewExerciseArray();
                        //newUser.exerciseData = new ExerciseData({missedKeys:[new MissedKey({missed: 't', typed:'r'}), new MissedKey({missed:'g', typed:'b'})]});

                        // save the user
                        newUser.save(function(err) {
                            if (err){
                                console.log('Error in Saving user: '+err);
                                throw err;
                            }
                            console.log('User Registration successful');
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
);

// Generates hash using bCrypt
var createHash = function(password)
{
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}
convertToUppercase = function(word)
{
    return word.charAt(0).toUpperCase() + word.slice(1);
};
convertToLowercase = function (word)
{
    return word.charAt(0).toLowerCase() + word.slice(1);
}
// passport/login.js
passport.use('login', new LocalStrategy(
    {
        passReqToCallback : true
    },
    function(req, email, password, done)
    {

        //username = convertToUppercase(username);
        // check in mongo if a user with username exists or not
        User.findOne({ 'email' :  email },
            function(err, user)
            {
                // In case of any error, return using the done method
                if (err)
                    return done(err);
                // Username does not exist, log error & redirect back
                if (!user)
                {
                    return done(null, false,
                        req.flash('message', 'Could not find that user.'));
                    res.redirect();
                }
                // User exists but wrong password, log the error
                if (!isValidPassword(user, password))
                {
                    return done(null, false,
                        req.flash('message', 'Invalid Password'));
                    res.redirect();
                }
                // User and password both match, return user from
                // done method which will be treated like success
                //req.session.return_to = '/#!dashboard';
                req.cookies['user'] = user;
                return done(null, user);
            }
        );
    }));
passport.serializeUser(function(user, done)
{
    done(null, user._id);
});

passport.deserializeUser(function(id, done)
{
    User.findOne({_id: id}, function(err, user)
    {

        done(err, user);
    });
});
var isValidPassword = function(user, password)
{
    return bCrypt.compareSync(password, user.password);
};

var updateExercise = function (exercises, newExercise)
{
    //console.log('NOOOOOOOO user exercises before update:: '+JSON.stringify(user.exerciseData.missedKeys));
    var tempSlot = 0;

    //for (var k = 0; k < exercises.length; k++)
    //{
    newExercise.avgCompletionTime = (Math.floor(newExercise.avgCompletionTime * 100)/100);
    newExercise.avgRate = (Math.floor(newExercise.avgRate * 100)/100);
    var k = (+newExercise.exercise.valueOf() - 1);
    var curEx = exercises[k];
        //console.log('length:'+user.exerciseData.length+'  - -- - --- --- - saving data:: '+JSON.stringify(user.exerciseData[k])+ '   with new data:: '+JSON.stringify(newExercise));
        //var tData = user.exerciseData[k];
        if (curEx.exercise === newExercise.exercise)
        {
            tempSlot = k;
            //var tempData = user.exerciseData[k];
            if (curEx.xCompleted > 0)
            {
                var tCompTime = curEx.avgCompletionTime;
                tCompTime = tCompTime * curEx.xCompleted;
                tCompTime = (+tCompTime + +newExercise.avgCompletionTime);
                tCompTime = tCompTime / (+curEx.xCompleted + 1);
                tCompTime = (Math.floor(tCompTime*100)/100);
                curEx.avgCompletionTime = tCompTime;

                var tRate = curEx.avgRate;
                tRate = tRate * curEx.xCompleted;
                tRate = +tRate + +newExercise.avgRate;
                tRate = tRate / (+curEx.xCompleted + 1);
                tRate = (Math.floor(tRate*100)/100);
                curEx.avgRate = tRate;

                var bComp = curEx.bestTime;
                if (+bComp === 0 || +newExercise.avgCompletionTime < +bComp )
                {
                    curEx.bestTime = newExercise.avgCompletionTime;
                }
                var bRate = curEx.bestRate;
                if (+newExercise.avgRate > +bRate)
                {
                    curEx.bestRate = newExercise.avgRate;
                }

                curEx.xCompleted++;
                //if ((exercises[k].xCompleted/5) === (Math.floor(exercises[k].xCompleted/5)))
                    exercises[k].history.push(
                        {
                            //exercise: exercises[k].exercise,
                            xCompleted: curEx.xCompleted,
                            bestTime: curEx.bestTime,
                            avgCompletionTime: curEx.avgCompletionTime,
                            bestRate: curEx.bestRate ,
                            avgRate: curEx.avgRate
                        });
                if (curEx.history.length > 5)
                    curEx.history.shift();
                curEx.missedKeys = newExercise.missedKeys;
                //user.exerciseData[k].missedKeys = newExercise.missedKeys;
                //for (var q = 0; q < newExercise.missedKeys.length; q++)
                //{
                //    addKeyToExercise(exercises[k], newExercise.missedKeys[q])
                //}

            }
            else
            {
                curEx.xCompleted = 1;
                curEx.avgCompletionTime = newExercise.avgCompletionTime;
                curEx.avgRate = newExercise.avgRate;
                curEx.bestTime = newExercise.avgCompletionTime;
                curEx.bestRate = newExercise.avgRate;

                exercises[k].history = [
                    {
                        exercise: newExercise.exercise,
                        xCompleted: 1,
                        bestTime: newExercise.avgCompletionTime,
                        avgCompletionTime: newExercise.avgCompletionTime,
                        bestRate: newExercise.avgRate,
                        avgRate: newExercise.avgRate
                    }];
                curEx.missedKeys = newExercise.missedKeys;
            }


            //break;
        }
    exercises[k] = curEx;
    return exercises;
};
global.saveExerciseData = function (req, res, id, exerciseData, missedKeys, done)
{
    //id = id.id;
    saveStartTime = new Date().getTime();
    console.log('Entering SaveExerciseData\n'+( saveStartTime )+'\n');
    User.findOne({_id:id}, function (err, user)
    {
        if (user)
        {

            console.log('Found User from mongo\n'+ (new Date().getTime() - saveStartTime)+'\n');
            var tempexercise;
            var texercise = {
                exercise: exerciseData.exercise,
                xCompleted: 1,
                avgCompletionTime: exerciseData.completionTime,
                avgRate: exerciseData.rate,
                missedKeys: exerciseData.missedKeys
            };
            tempexercise = user.exerciseData[exerciseData.exercise-1];
            //for (var i = 0; i < user.exerciseData.length; i++)
            //{
            //    if (user.exerciseData[i].exercise === exerciseData.exercise)
            //    {
            //        break;
            //    }
            //}
            //asdfasdf;;;;;

            //console.log('----- pushing completion time:: '+exerciseData.completionTime+'  ------- and rate:: ' +exerciseData.rate);
            var sexyarray = missedKeys;//JSON.parse(missedKeys);
            //var texyarray = [];
            for (var j = 0; j < tempexercise.missedKeys.length; j++)
            {
                if(tempexercise.missedKeys[j].history)
                    tempexercise.missedKeys[j].history += 1;
                else
                    tempexercise.missedKeys[j].history = 1;
            }
            for (var k = 0; k <= sexyarray.length; k++)
            {
                if (k === sexyarray.length)
                    texercise.missedKeys = tempexercise.missedKeys;
                else
                    texercise.missedKeys = addKeyToExercise(tempexercise, sexyarray[k]);
                //texercise.missedKeys = getNewKeyArray();

                //console.log('texercise has missedkeys of ::: ' + JSON.stringify(texercise.missedKeys[0]     ));

                //console.log('------tried to push keys: : ' + JSON.stringify(sexyarray[k]) +' --- at index k==='+k);
            }
            //console.log('  - -- - --- --- - saving data:: '+JSON.stringify(user.exerciseData)+ '   avgRate:: '+user.exerciseData[0].avgRate);
            for (var l = 0; l < texercise.missedKeys.length; l++)
            {
                if (texercise.missedKeys[l].history >= 5)
                {
                    texercise.missedKeys[l].typed = [];
                    texercise.missedKeys[l].errorCount = 0;
                    //texercise.missedKeys[l].history = 0;
                }
            }
            //user = updateExercise(user, texercise);
            //console.log('++++++++++++++ user exercise data, before updating exercise::: ' + JSON.stringify(user.exerciseData) + '\n+++++++++++++ user avgCompletionTime ::: ' + user.exerciseData[0].avgCompletionTime);
            user.exerciseData = updateExercise(user.exerciseData, texercise);
            user.score += exerciseData.score;
            //console.log('tExerciseData:: '+ JSON.stringify(texercise));
            //console.log('$$$ about to save the user with this data::: '+JSON.stringify(texercise));

            console.log('finished exsave data calculations\n'+ (new Date().getTime()-saveStartTime)+'\n');
            user.save(function (err) {
                if (err) {
                    //console.log('Error in Saving user: ' + err);
                    throw err;
                }
                //console.log('exercise data saved successfully');

                console.log('Saved the user\n'+ (new Date().getTime()-saveStartTime)+'\n');
                return res.end('Saved Exercise Data Successfully');

                //return 'done';
            });
            //console.log('users final exerciselist:: '+JSON.stringify(user.exerciseData));
            //console.log('finished saving the user');
            //done(err, user);
        }
        else if (err){
            return res.end('Failed Saving Exercise Data');
        }
    });
    //process.nextTick(passport.sa);
};

var addKeyToExercise = function(ex, missedKey){

    for (var k = 0; k < ex.missedKeys.length; k++)
    {
        var curMissedKey = ex.missedKeys[k];
        if (curMissedKey.missed === missedKey.missed)
        {
            var foundExisting = false;
            curMissedKey.errorCount++;
            //if (ex.missedKeys[k].history >= 1)
            //if (ex.misedKeys[k].typed.length != 0)
            curMissedKey.history = 0;

            for (var j = 0; j < curMissedKey.typed.length; j++)
            {
                if (curMissedKey.typed[j] === missedKey.typed)
                {
                    foundExisting = true;
                    //console.log('updating the missed key::: '+missedKey.typed);
                    break;
                }
            }
            if (!foundExisting)
            {
                curMissedKey.typed.push( missedKey.typed );
                //console.log('Did not find existing mistyped key in "addKeyToExercise()"   ' + ex.missedKeys[k].missed + '::'+ex.missedKeys[k].typed+' || '+missedKey.typed);
                //ex.missedKeys[k].errorCount++;
            }
            ex.missedKeys[k] = curMissedKey;
        }
    }
    //console.log('fetched users exercise data::::: '+JSON.stringify(ex));
    return ex.missedKeys;
};
//app.options.agent = false;
http.globalAgent.maxSockets += 250;
//http.createServer(app).listen(app.get('port'));
var keynodeserver = app.listen(app.get('port'), function () {
console.log('Node.js Express running on: '+ app.get('port'));
    var host = keynodeserver.address().address;
    var port = keynodeserver.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});
module.exports = app;