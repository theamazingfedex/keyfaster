var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var missedKeySchema = new Schema({
    missed: String,
    typed: Array,
    errorCount: Number
});

module.exports = mongoose.model('missedkeys', missedKeySchema);

