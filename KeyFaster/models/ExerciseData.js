var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var missedKeySchema = require('./MissedKey.js');
var missedKeySchema = new Schema({
    missed: String,
    typed: Array,
    errorCount: Number
});
var getNewKeyArray = function()
{
    var mskeys = [];
    mskeys.push({missed:'space', typed:[], errorCount: 0});
    mskeys.push({missed:'a', typed:[], errorCount: 0});
    mskeys.push({missed:'b', typed:[], errorCount: 0});
    mskeys.push({missed:'c', typed:[], errorCount: 0});
    mskeys.push({missed:'d', typed:[], errorCount: 0});
    mskeys.push({missed:'e', typed:[], errorCount: 0});
    mskeys.push({missed:'f', typed:[], errorCount: 0});
    mskeys.push({missed:'g', typed:[], errorCount: 0});
    mskeys.push({missed:'h', typed:[], errorCount: 0});
    mskeys.push({missed:'i', typed:[], errorCount: 0});
    mskeys.push({missed:'j', typed:[], errorCount: 0});
    mskeys.push({missed:'k', typed:[], errorCount: 0});
    mskeys.push({missed:'l', typed:[], errorCount: 0});
    mskeys.push({missed:'m', typed:[], errorCount: 0});
    mskeys.push({missed:'n', typed:[], errorCount: 0});
    mskeys.push({missed:'o', typed:[], errorCount: 0});
    mskeys.push({missed:'p', typed:[], errorCount: 0});
    mskeys.push({missed:'q', typed:[], errorCount: 0});
    mskeys.push({missed:'r', typed:[], errorCount: 0});
    mskeys.push({missed:'s', typed:[], errorCount: 0});
    mskeys.push({missed:'t', typed:[], errorCount: 0});
    mskeys.push({missed:'u', typed:[], errorCount: 0});
    mskeys.push({missed:'v', typed:[], errorCount: 0});
    mskeys.push({missed:'w', typed:[], errorCount: 0});
    mskeys.push({missed:'x', typed:[], errorCount: 0});
    mskeys.push({missed:'y', typed:[], errorCount: 0});
    mskeys.push({missed:'z', typed:[], errorCount: 0});
    mskeys.push({missed:'A', typed:[], errorCount: 0});
    mskeys.push({missed:'B', typed:[], errorCount: 0});
    mskeys.push({missed:'C', typed:[], errorCount: 0});
    mskeys.push({missed:'D', typed:[], errorCount: 0});
    mskeys.push({missed:'E', typed:[], errorCount: 0});
    mskeys.push({missed:'F', typed:[], errorCount: 0});
    mskeys.push({missed:'G', typed:[], errorCount: 0});
    mskeys.push({missed:'H', typed:[], errorCount: 0});
    mskeys.push({missed:'I', typed:[], errorCount: 0});
    mskeys.push({missed:'J', typed:[], errorCount: 0});
    mskeys.push({missed:'K', typed:[], errorCount: 0});
    mskeys.push({missed:'L', typed:[], errorCount: 0});
    mskeys.push({missed:'M', typed:[], errorCount: 0});
    mskeys.push({missed:'N', typed:[], errorCount: 0});
    mskeys.push({missed:'O', typed:[], errorCount: 0});
    mskeys.push({missed:'P', typed:[], errorCount: 0});
    mskeys.push({missed:'Q', typed:[], errorCount: 0});
    mskeys.push({missed:'R', typed:[], errorCount: 0});
    mskeys.push({missed:'S', typed:[], errorCount: 0});
    mskeys.push({missed:'T', typed:[], errorCount: 0});
    mskeys.push({missed:'U', typed:[], errorCount: 0});
    mskeys.push({missed:'V', typed:[], errorCount: 0});
    mskeys.push({missed:'W', typed:[], errorCount: 0});
    mskeys.push({missed:'X', typed:[], errorCount: 0});
    mskeys.push({missed:'Y', typed:[], errorCount: 0});
    mskeys.push({missed:'Z', typed:[], errorCount: 0});

    return mskeys;
};


var exerciseDataSchema;
exerciseDataSchema = new Schema({
    exercise: String,
    xCompleted: Number,
    bestTime: Number,
    avgCompletionTime: Number,
    bestRate: Number,
    avgRate: Number,
    missedKeys: {type: [missedKeySchema], default: getNewKeyArray}
});

//exerciseDataSchema.methods.updateExercise = function (newExercise)
//{
//    //console.log('NOOOOOOOO exercises before update:: '+JSON.stringify(this.exerciseData));
//    for (var k = 0; k < this.exerciseData.length; k++)
//    {
//        var tData = this.exerciseData[k];
//        if (this.exerciseData[k].exercise === newExercise.exercise)
//        {
//            if (tData.xCompleted > 0)
//            {
//                var tCompTime = tData.avgCompletionTime;
//                tCompTime *= tData.xCompleted;
//                tCompTime += newExercise.avgCompletionTime;
//                tCompTime /= (this.xCompleted + 1);
//
//                tData.avgCompletionTime = tCompTime;
//                var tRate = tData.avgRate;
//                tRate *= tData.xCompleted;
//                tRate += newExercise.avgRate;
//                tRate /= (this.xCompleted + 1);
//
//                tData.avgRate = tRate;
//                tData.xCompleted++;
//
//                for (var i = 0; i < newExercise.missedKeys.length; i++)
//                {
//                    this.addKey(tData, newExercise.missedKeys[i]);
//                }
//                //this.missedKeys.join(newExercise.missedKeys);
//
//            }
//            else
//            {
//                tData.xCompleted = 1;
//                tData.avgCompletionTime = newExercise.avgCompletionTime;
//                tData.avgRate = newExercise.avgRate;
//                tData.missedKeys = newExercise.missedKeys;
//                tData.exercise = newExercise.exercise;
//            }
//        }
//    }
//    this.exerciseData = tData;
//    //console.log('NOOOOOO exercises AFTER update:: '+JSON.stringify(this.exerciseData));
//};


exerciseDataSchema.methods.addKey = function(missedKey){
    for (var k = 0; k < ex.missedKeys.length; k++)
    {
        if (ex.missedKeys[k].missed == missedKey.missed)
        {
            var foundExisting = false;
            ex.missedKeys[k].errorCount += 1;
            for (var j = 0; j < ex.missedKeys[k].typed.length; j++)
            {
                if (ex.missedKeys[k].typed[j] === missedKey.typed)
                {
                    foundExisting = true;
                    break;
                }
            }
            if (!foundExisting)
            {
                ex.missedKeys[k].typed.push(missedKey.typed);
            }
        }
    }
    return ex.missedKeys;
};
//exerciseDataSchema.methods.getNewKeyArray = function()
//{
//    var mskeys = [];
//    mskeys.push(missedKeySchema({missed:'space', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'a', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'b', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'c', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'d', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'e', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'f', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'g', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'h', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'i', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'j', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'k', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'l', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'m', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'n', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'o', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'p', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'q', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'r', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'s', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'t', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'u', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'v', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'w', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'x', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'y', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'z', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'A', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'B', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'C', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'D', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'E', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'F', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'G', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'H', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'I', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'J', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'K', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'L', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'M', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'N', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'O', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'P', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'Q', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'R', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'S', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'T', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'U', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'V', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'W', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'X', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'Y', typed:[], errorCount: 0}));
//    mskeys.push(missedKeySchema({missed:'Z', typed:[], errorCount: 0}));
//
//    return mskeys;
//};
//module.exports = getNewKeyArray;
module.exports = mongoose.model('exercisedata', exerciseDataSchema);