//UserModel setup
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
//var exerciseDataSchema = require('./ExerciseData.js');


var getNewKeyArray = function()
{
    var mskeys = [
        {missed:'1', typed:[], history: 0, errorCount: 0},
        {missed:'2', typed:[], history: 0, errorCount: 0},
        {missed:'3', typed:[], history: 0, errorCount: 0},
        {missed:'4', typed:[], history: 0, errorCount: 0},
        {missed:'5', typed:[], history: 0, errorCount: 0},
        {missed:'6', typed:[], history: 0, errorCount: 0},
        {missed:'7', typed:[], history: 0, errorCount: 0},
        {missed:'8', typed:[], history: 0, errorCount: 0},
        {missed:'9', typed:[], history: 0, errorCount: 0},
        {missed:'0', typed:[], history: 0, errorCount: 0},
        {missed:'space', typed:[], history:0, errorCount: 0},
        {missed:'a', typed:[], history:0, errorCount: 0},
        {missed:'b', typed:[], history:0, errorCount: 0},
        {missed:'c', typed:[], history:0, errorCount: 0},
        {missed:'d', typed:[], history:0, errorCount: 0},
        {missed:'e', typed:[], history:0, errorCount: 0},
        {missed:'f', typed:[], history:0, errorCount: 0},
        {missed:'g', typed:[], history:0, errorCount: 0},
        {missed:'h', typed:[], history:0, errorCount: 0},
        {missed:'i', typed:[], history:0, errorCount: 0},
        {missed:'j', typed:[], history:0, errorCount: 0},
        {missed:'k', typed:[], history:0, errorCount: 0},
        {missed:'l', typed:[], history:0, errorCount: 0},
        {missed:'m', typed:[], history:0, errorCount: 0},
        {missed:'n', typed:[], history:0, errorCount: 0},
        {missed:'o', typed:[], history:0, errorCount: 0},
        {missed:'p', typed:[], history:0, errorCount: 0},
        {missed:'q', typed:[], history:0, errorCount: 0},
        {missed:'r', typed:[], history:0, errorCount: 0},
        {missed:'s', typed:[], history:0, errorCount: 0},
        {missed:'t', typed:[], history:0, errorCount: 0},
        {missed:'u', typed:[], history:0, errorCount: 0},
        {missed:'v', typed:[], history:0, errorCount: 0},
        {missed:'w', typed:[], history:0, errorCount: 0},
        {missed:'x', typed:[], history:0, errorCount: 0},
        {missed:'y', typed:[], history:0, errorCount: 0},
        {missed:'z', typed:[], history:0, errorCount: 0},
        {missed:'A', typed:[], history:0, errorCount: 0},
        {missed:'B', typed:[], history:0, errorCount: 0},
        {missed:'C', typed:[], history:0, errorCount: 0},
        {missed:'D', typed:[], history:0, errorCount: 0},
        {missed:'E', typed:[], history:0, errorCount: 0},
        {missed:'F', typed:[], history:0, errorCount: 0},
        {missed:'G', typed:[], history:0, errorCount: 0},
        {missed:'H', typed:[], history:0, errorCount: 0},
        {missed:'I', typed:[], history:0, errorCount: 0},
        {missed:'J', typed:[], history:0, errorCount: 0},
        {missed:'K', typed:[], history:0, errorCount: 0},
        {missed:'L', typed:[], history:0, errorCount: 0},
        {missed:'M', typed:[], history:0, errorCount: 0},
        {missed:'N', typed:[], history:0, errorCount: 0},
        {missed:'O', typed:[], history:0, errorCount: 0},
        {missed:'P', typed:[], history:0, errorCount: 0},
        {missed:'Q', typed:[], history:0, errorCount: 0},
        {missed:'R', typed:[], history:0, errorCount: 0},
        {missed:'S', typed:[], history:0, errorCount: 0},
        {missed:'T', typed:[], history:0, errorCount: 0},
        {missed:'U', typed:[], history:0, errorCount: 0},
        {missed:'V', typed:[], history:0, errorCount: 0},
        {missed:'W', typed:[], history:0, errorCount: 0},
        {missed:'X', typed:[], history:0, errorCount: 0},
        {missed:'Y', typed:[], history:0, errorCount: 0},
        {missed:'Z', typed:[], history:0, errorCount: 0}
    ];

    return mskeys;
};
var historyDataSchema = new Schema(
    {
        exercise: String,
        xCompleted: Number,
        bestTime: Number,
        avgCompletionTime: Number,
        bestRate: Number,
        avgRate: Number
    });
var missedKeySchema = new Schema(
    {
        missed: String,
        typed: Array,
        history: Number,
        errorCount: Number
    });
var exerciseDataSchema;
exerciseDataSchema = new Schema(
    {
        exercise: String,
        xCompleted: Number,
        bestTime: Number,
        avgCompletionTime: Number,
        bestRate: Number,
        avgRate: Number,
        history: {type: [historyDataSchema], default: []},
        missedKeys: {type: [missedKeySchema], default: getNewKeyArray()}
    });




exerciseDataSchema.methods.addKey = function(ex, missedKey){
    //ex = this;
    for (var k = 0; k < ex.missedKeys.length; k++)
    {
        if (ex.missedKeys[k].missed == missedKey.missed)
        {
            var foundExisting = false;
            ex.missedKeys[k].errorCount += 1;
            for (var j = 0; j < ex.missedKeys[k].typed.length; j++)
            {
                if (ex.missedKeys[k].typed[j] === missedKey.typed)
                {
                    foundExisting = true;
                    break;
                }
            }
            if (!foundExisting)
            {
                ex.missedKeys[k].typed.push(missedKey.typed);
            }
        }
    }
    return ex.missedKeys;
};
exerciseDataSchema.methods.getNewKeyArray = function()
{
    var mskeys = [];
    mskeys.push({missed:'space', typed:[], errorCount: 0});
    mskeys.push({missed:'a', typed:[], errorCount: 0});
    mskeys.push({missed:'b', typed:[], errorCount: 0});
    mskeys.push({missed:'c', typed:[], errorCount: 0});
    mskeys.push({missed:'d', typed:[], errorCount: 0});
    mskeys.push({missed:'e', typed:[], errorCount: 0});
    mskeys.push({missed:'f', typed:[], errorCount: 0});
    mskeys.push({missed:'g', typed:[], errorCount: 0});
    mskeys.push({missed:'h', typed:[], errorCount: 0});
    mskeys.push({missed:'i', typed:[], errorCount: 0});
    mskeys.push({missed:'j', typed:[], errorCount: 0});
    mskeys.push({missed:'k', typed:[], errorCount: 0});
    mskeys.push({missed:'l', typed:[], errorCount: 0});
    mskeys.push({missed:'m', typed:[], errorCount: 0});
    mskeys.push({missed:'n', typed:[], errorCount: 0});
    mskeys.push({missed:'o', typed:[], errorCount: 0});
    mskeys.push({missed:'p', typed:[], errorCount: 0});
    mskeys.push({missed:'q', typed:[], errorCount: 0});
    mskeys.push({missed:'r', typed:[], errorCount: 0});
    mskeys.push({missed:'s', typed:[], errorCount: 0});
    mskeys.push({missed:'t', typed:[], errorCount: 0});
    mskeys.push({missed:'u', typed:[], errorCount: 0});
    mskeys.push({missed:'v', typed:[], errorCount: 0});
    mskeys.push({missed:'w', typed:[], errorCount: 0});
    mskeys.push({missed:'x', typed:[], errorCount: 0});
    mskeys.push({missed:'y', typed:[], errorCount: 0});
    mskeys.push({missed:'z', typed:[], errorCount: 0});
    mskeys.push({missed:'A', typed:[], errorCount: 0});
    mskeys.push({missed:'B', typed:[], errorCount: 0});
    mskeys.push({missed:'C', typed:[], errorCount: 0});
    mskeys.push({missed:'D', typed:[], errorCount: 0});
    mskeys.push({missed:'E', typed:[], errorCount: 0});
    mskeys.push({missed:'F', typed:[], errorCount: 0});
    mskeys.push({missed:'G', typed:[], errorCount: 0});
    mskeys.push({missed:'H', typed:[], errorCount: 0});
    mskeys.push({missed:'I', typed:[], errorCount: 0});
    mskeys.push({missed:'J', typed:[], errorCount: 0});
    mskeys.push({missed:'K', typed:[], errorCount: 0});
    mskeys.push({missed:'L', typed:[], errorCount: 0});
    mskeys.push({missed:'M', typed:[], errorCount: 0});
    mskeys.push({missed:'N', typed:[], errorCount: 0});
    mskeys.push({missed:'O', typed:[], errorCount: 0});
    mskeys.push({missed:'P', typed:[], errorCount: 0});
    mskeys.push({missed:'Q', typed:[], errorCount: 0});
    mskeys.push({missed:'R', typed:[], errorCount: 0});
    mskeys.push({missed:'S', typed:[], errorCount: 0});
    mskeys.push({missed:'T', typed:[], errorCount: 0});
    mskeys.push({missed:'U', typed:[], errorCount: 0});
    mskeys.push({missed:'V', typed:[], errorCount: 0});
    mskeys.push({missed:'W', typed:[], errorCount: 0});
    mskeys.push({missed:'X', typed:[], errorCount: 0});
    mskeys.push({missed:'Y', typed:[], errorCount: 0});
    mskeys.push({missed:'Z', typed:[], errorCount: 0});

    return mskeys;
};
var getNewExerciseArray = function () {
    var exerciseArray = [];
    var newKeys = getNewKeyArray();
    for (var i = 0; i < 30; i++)
    {
        exerciseArray[i]= {exercise:1+i,xCompleted:0, avgCompletionTime:0,bestTime:0,avgRate:0,bestRate:0,history:[],missedKeys:newKeys};
    }
    //console.log('printing the User.js version of exerciseArray:: '+JSON.stringify(exerciseArray[2]));
    return exerciseArray;
};

var userSchema = new Schema({
    username: String,
    password: String,
    email: String,
    theme: String,
    psc: Number,
    tpid: Number,
    provider: String,
    providerId:String,
    providerData: {},
    joindate: { type: Date, default: Date.now },
    exerciseData: {type:[exerciseDataSchema], default:getNewExerciseArray()} //Array//[{type: exerciseDataSchema}]
});
////userSchema.methods.updateExercise = function (newExercise)
////{
////    var tData;
////    for (var k = 0; k < this.exerciseData.length; k++)
////    {
////        //console.log('exercises before update:: '+JSON.stringify(this.exerciseData[k]));
////        tData = this.exerciseData[k];
////        if (this.exerciseData[k].exercise === newExercise.exercise)
////        {
////            if (tData.xCompleted > 0)
////            {
////                var tCompTime = tData.avgCompletionTime;
////                tCompTime = tCompTime * tData.xCompleted;
////                tCompTime = tCompTime + newExercise.avgCompletionTime;
////                tCompTime = tCompTime / (tData.xCompleted + 1);
////
////                tCompTime =(Math.floor(tCompTime* 100)/100);
////                tData.avgCompletionTime = tCompTime;
////
////                var tRate = tData.avgRate;
////                tRate = tRate * tData.xCompleted;
////                tRate = tRate + newExercise.avgRate;
////                tRate = tRate / (tData.xCompleted + 1);
////
////                tRate = (Math.floor(tRate * 100)/100);
////                tData.avgRate = tRate;
////                tData.xCompleted++;
////
////                for (var i = 0; i < newExercise.missedKeys.length; i++)
////                {
////                    this.exerciseData[k] = exerciseDataSchema.methods.addKey(this.exerciseData[k], newExercise.missedKeys[i]);
////                }
////                //this.missedKeys.join(newExercise.missedKeys);
////
////            }
////            else
////            {
////                tData.xCompleted = 1;
////                tData.avgCompletionTime = newExercise.avgCompletionTime;
////                tData.avgRate = newExercise.avgRate;
////                tData.missedKeys = newExercise.missedKeys;
////                tData.exercise = newExercise.exercise;
////            }
////        }
////        this.exerciseData = tData;
////    }
////    //console.log('exercises AFTER update:: '+JSON.stringify(this.exerciseData));
////};
//userSchema.methods.getNewExerciseArray = function () {
//    var exerciseArray = [];
//    for (var p = 0; p < 19; p++)
//    {
//        exerciseArray[p] = {exercise:1+p, avgCompletionTime:0, avgRate:0, xCompleted:0, missedKeys:getNewKeyArray()};
//        console.log(p);
//    }
//    return exerciseArray;
//};

userSchema.methods.addData = function (data) {

    this.exerciseData[data.exercise-1].updateExercise(data);
    return this.exerciseData;
};

userSchema.methods.generateHash = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('users', userSchema);